//
//  SCLoginLogicManager.m
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCLoginLogicManager.h"
#import "SCLoginVC.h"
#import "SCRootViewController.h"


@implementation SCLoginLogicManager
// 有无网络  有无自动登录 程序刚启动
+(void)startAppAtDelegate:(id)delegate{

    double interVal =-24*60*60;
   
     globleDate = [SCShareFunc stringFromDate:[NSDate dateWithTimeInterval:interVal sinceDate:[NSDate date]]]; //默认比系统时间 早一天 因为logic 数据计算都是从前一天的12:00到今天的12:00
    systemDate =[SCShareFunc stringFromDate:[NSDate date]];
    
    UIViewController *loginVC =nil;
    NSString *nibName =nil;
    if ([SCShareFunc isIPhone]) {
        nibName =@"SCLoginVC_iPhone";
    }else{
        nibName =@"SCLoginVC_iPad";
    }
//     nibName =@"SCLoginVC_iPhone"; //gyc just use iphone for test before release
    loginVC =[[SCLoginVC  alloc] initWithNibName:nibName bundle:nil];
    
    ((SCAppDelegate *)delegate).window.rootViewController =loginVC;
}


+(void)changeWindowRootVCToAfterLogin:(BOOL)afterlogin orToLoginVC:(BOOL)login{
    if ((afterlogin&&login)||(!afterlogin&&!login)) {
        NSLog(@"逻辑切换错误");
        return;
    }
    UIViewController *rootVC =nil;
    NSString *nibName =nil;
    if (afterlogin) {
        
        if ([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:NSClassFromString(@"SCRootViewController")]) {
            return;
        };
        
        if ([SCShareFunc isIPhone]) {
            nibName =@"SCRootViewController_iPhone";
        }else{
            nibName =@"SCRootViewController_iPad";
        }
    }
    if (login) {
        if ([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:NSClassFromString(@"SCLoginVC")]) {
            return;
        };
        
        if ([SCShareFunc isIPhone]) {
            nibName =@"SCLoginVC_iPhone";
        }else{
            nibName =@"SCLoginVC_iPad";
        }
//        nibName =@"SCLoginVC_iPhone"; //gyc just use iphone for test before release
    }
    [UIApplication sharedApplication].keyWindow.rootViewController =nil;
    if (afterlogin) {
        rootVC =[[SCRootViewController alloc] initWithNibName:nibName bundle:nil];
    }else{
        rootVC =[[SCLoginVC alloc] initWithNibName:nibName bundle:nil];
    }
    
    [UIApplication sharedApplication].keyWindow.rootViewController =rootVC;
    
}

@end

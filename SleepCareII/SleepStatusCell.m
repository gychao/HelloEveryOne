//
//  SleepStatusCell.m
//  SleepCareII
//
//  Created by mengqinghao on 14-6-24.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SleepStatusCell.h"
#import "SCSleepEStatusView.h"

@implementation SleepStatusCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)layoutSubviews{
    [super layoutSubviews];
    if ([SCShareFunc isIPhone]) {
    
        float numCount =7;
        float sperTor =1;
        float stance =(self.bounds.size.width)/numCount;
        
        for (int i=0; i<numCount-1; i++) {
            UIView *imageView =[self.contentView viewWithTag:i+10];
            imageView.frame =(CGRect){stance*(i+1)-sperTor/2,imageView.frame.origin.y,sperTor,imageView.frame.size.height};// 分割线
            
            if (i==0) {
                CGRect frame=  self.drawSleepInBed.frame;
                frame.origin.x =stance*(i+1);
                frame.size.width =self.bounds.size.width -imageView.frame.origin.x;
                self.drawSleepInBed.frame =frame;
            }
            
        }
        

            //        self.dateTimeLabel.font =[UIFont systemFontOfSize:10];
            self.dateTimeLabel.adjustsFontSizeToFitWidth =YES;
            self.dateTimeLabel.textAlignment =NSTextAlignmentLeft;
            
            CGRect frame =self.spertorView.frame;
            frame.origin.y =self.contentView.bounds.size.height -1;
            frame.size.height =0.5;
            self.spertorView.frame =frame;
            
    

    }
}



-(void)displayTime:(NSArray*)Timeary dateTime:(NSString*)date;
{
    self.dateTimeLabel.text=date;
    NSDictionary *dic =((Timeary==nil)?nil:@{@"inBedTimeList":Timeary});
    [self.drawSleepInBed  drawSelfUseDataDic:dic withAnotherUsePara:nil];
    self.spertorView.hidden =NO;
    
}

@end

//
//  SCSleepEvaluateVC.h
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"

#define statusIdentify  @"status"   // 状态
#define detail_xt  @"detail_xt"     // xt
#define detail_hx  @"detail_hx"     // hx
#define  detail_sm @"detail_sm"     // sm
#define SleepLevel      @"level"    // 睡眠阶段


@interface SCSleepEvaluateVC : SCBaseViewController

@property (weak, nonatomic) IBOutlet UIButton *statusBtn;
@property (weak, nonatomic) IBOutlet UIButton *detailsBtn;

@property(nonatomic,strong)UIScrollView *scroView;
@property(nonatomic,strong)NSMutableArray  *visibleViews;

// this func has no use
-(void)displayScrollViewWithDic:(NSDictionary *)dataDic;

-(void)dataToCalander;

@end

//
//  UILabel+DrawTextAlignment.h
//  SleepCareII
//
//  Created by dilitech on 14-7-4.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//
//
#import <UIKit/UIKit.h>

typedef enum{
    VerticalAlignmentTop = 0,
    VerticalAlignmentMidele,
    VerticalAlignmentBottom,
    VerticalAlignmentMax
}VerticalAlignment;


@interface UILabel (DrawTextAlignment)
- (void)setVerticalAlignment:(VerticalAlignment)verticalAlignment;
@end

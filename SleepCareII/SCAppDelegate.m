//
//  SCAppDelegate.m
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCAppDelegate.h"
#import  "SCLoginLogicManager.h"
#import "DXAlertView.h"

@implementation SCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
//    NSString *name =[SCShareFunc nameWithInstance:self.window andInstanceOwner:self];
    
   
   
    [self performSelector:@selector(startApp:) withObject:nil afterDelay:0];
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes: UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert];
    //zdf add 2014-04-29 用户主动关闭推送通知时，必须提醒用户手动开启
    if ([[UIApplication sharedApplication] enabledRemoteNotificationTypes]==UIRemoteNotificationTypeNone) {
        [[[DXAlertView alloc] initWithTitle:nil contentText:@"请在i\"设置->通知中心\"中,将本程序推送打开,否则无法收到提醒信息."leftButtonTitle:nil rightButtonTitle:nil]  show];
        
       // [ShareMethod alertViewTitle:nil message:@"请在i\"设置->通知中心\"中,将本程序推送打开,否则无法收到提醒信息." cancelButtonTitle:NSLocalizedString(@"OK", nil)];
    }

    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}
-(void)startApp:(id)obj{
    if ([SCShareFunc isIPhone]) {
        isIPhonePlat =YES;
    }else{
        isIPhonePlat =NO;
    }
    [SCLoginLogicManager startAppAtDelegate:self];
}

#pragma mark - RomoteNotification 远程推送
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)pToken {
    NSLog(@"regisger success:%@", pToken);
    //注册成功，将deviceToken保存到应用服务器数据库中
    NSString * str =[NSString stringWithFormat:@"%@",pToken];
    NSString *tmpstr = [str substringWithRange:NSMakeRange(1, 71)];
    NSString * parsecode =[tmpstr stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[NSUserDefaults standardUserDefaults] setObject:parsecode forKey:@"Token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    
    NSUserDefaults  * stand =[NSUserDefaults standardUserDefaults];
    NSString * parsecode = [stand objectForKey:@"Token"];
    if (!parsecode) {
      [[[DXAlertView alloc] initWithTitle:nil contentText:@"请在\"设置->通知中心\"中,将本程序推送打开,否则无法收到提醒信息."leftButtonTitle:nil rightButtonTitle:nil]  show];
    }
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    // here is application is running or in background
//    NSLog(@"%@",userInfo);
//    NSString *title =[[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
//    if (title.length>0) {
//        [ShareMethod alertViewTitle:nil message:title cancelButtonTitle:@"确定"];
//    }
//    application.applicationIconBadgeNumber =0;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

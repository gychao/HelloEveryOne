//
//  SCCurrentStatusView.m
//  SleepCareII
//
//  Created by dilitech on 14-6-16.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCCurrentStatusView.h"
#import "SCLast20Cell.h"

@interface SCCurrentStatusView ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,assign)int numIndex;
@end

@implementation SCCurrentStatusView



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)dealloc
{
    NSLog(@"%s",__func__);
}

-(void)displayViewUse:(NSDictionary *)reponse withNumTag:(int)numIndex{
    NSLog(@"ye");
    // 让这个csv 去加载
    NSString *type =nil;
    self.numIndex =numIndex;
    if (1==numIndex) {
        type =KgetCurrentSleepStatus;
    }else if (2==numIndex){
        type =KgetLast20MinSleepStatusDetail;
    }
    
    // gyc 先用空数据 刷新一次
    [self redisplaySelfByDic:nil];
    //  end
    
  
    [SCNetManager getSleepActivityInfoSuccess:^(BOOL secusss,NSDictionary *reponse){
        
        [self redisplaySelfByDic:reponse];
        
    } faileture:^(BOOL failture){
        [self redisplaySelfByDic:nil];
    } withType:type andWithPeroid:0];

}


-(UITableView *)_20minTableView{
    if (!__20minTableView) {
        CGRect rect =(CGRect){0,self.TopLabelBreath.bounds.size.height,self.bounds.size.width,self.bounds.size.height-self.TopLabelBreath.bounds.size.height};
        __20minTableView =[[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
        __20minTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
        __20minTableView.backgroundColor =[UIColor clearColor];
        __20minTableView.backgroundView =nil;
        __20minTableView.delegate =self;
        __20minTableView.dataSource =self;
        
        self._20minTableView =__20minTableView;
        [self addSubview:__20minTableView];
    }
    return __20minTableView;
}
-(void)layoutSubviews{
   
    self.currentView.frame =(CGRect){0,0,self.frame.size.width,self.frame.size.height};
    NSLog(@"%@",NSStringFromCGRect(self.currentView.frame));
    if (self.currentView.superview ==nil) {
        [self insertSubview:self.currentView atIndex:0];
    }
}

#pragma mark - really refresh
-(void)redisplaySelfByDic:(NSDictionary *)dic{
    [self.subviews makeObjectsPerformSelector:@selector(setHidden:) withObject:@YES];
#warning gyc imagename
    switch (self.numIndex) {
        case 1:
        {
            self.currentView.hidden =NO;
            NSNumber *num =[dic objectForKey:@"isInBed"];
            if (num&&[num isKindOfClass:[NSNumber class]]) {
                if ([num intValue]) {
                    if (isIPhonePlat) {
                        if (isIPhone5) {
                            self.bgImageView.image =[UIImage imageNamed:@"_4zaichuang"];
                            self.breath.frame =(CGRect){90,169,85,23};
                            self.heartbeat.frame =(CGRect){295,169,90,23};
                            // 此坐标 是实际xib 测试得知，并非有什么原因
                        }else{
                            self.bgImageView.image =[UIImage imageNamed:@"_4zaichuang"];
                        }
                       
                    }else{
                        self.bgImageView.image =[UIImage imageNamed:@"testdangqianzhuangtai"];
                    }
                    
                }else{
                    if (isIPhonePlat) {
                        
                        self.bgImageView.image =[UIImage imageNamed:@"_4lichuang"];
                        if (isIPhone5) {
                            self.bgImageView.image =[UIImage imageNamed:@"_5lichuang"];
                    }
                        
                    }else{
                        self.bgImageView.image =[UIImage imageNamed:@"lichuang"];

                   }
                }
            }

            NSArray *labelArr =@[_breath,_heartbeat];
            for (UILabel *label in labelArr) {
                [self getText:label fromDataDic:dic];
            }
            
        }
            break;
        case 2:{
            // 20 min
            self.TopLabelBreath.hidden =self.TopLabelBreathStop.hidden =self.TopLabelHeartBeat.hidden =self.TopLabelTime.hidden =NO;
            self._20minTableView.hidden =NO;
            self.dataArr =[dic objectForKey:@"details"];
            [self._20minTableView reloadData];
        }
            
        default:
            break;
    }

}



-(void)getText:(UILabel *)label fromDataDic:(NSDictionary *)dic{
    if (!label) {
        return;
    }
    NSString *key =[SCShareFunc nameWithInstance:label andInstanceOwner:self];
    NSString *value =[[dic objectForKey:key] stringValue];
    
    if (![SCShareFunc isNotEmptyStringOfObj:value]||([value intValue]<0)) {
        label.hidden =YES;
        return;
    }
    label.hidden =NO;
 
    label.text =[NSString stringWithFormat:@"%@次",value];
    label.hidden =NO;
}

#pragma mark -tableview
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SCLast20Cell *cell =[tableView dequeueReusableCellWithIdentifier:@"_20Cell"];
    if (!cell) {
        cell =[[SCLast20Cell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:@"_20Cell"];
    }
    
    [cell displayViewUse:[self.dataArr objectAtIndex:indexPath.row] andRow:indexPath.row];
    
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    return cell;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

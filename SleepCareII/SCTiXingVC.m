//
//  SCTiXingVC.m
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCTiXingVC.h"
#import "NoticeCell.h"
#import "SCNetManager.h"
#import "SCTiXingDetailVC.h"

#import "SCRootViewController.h"

#import "DXAlertView.h"

@interface SCTiXingVC ()<DXAlertViewDelegate>

@end

@implementation SCTiXingVC

@synthesize deleteAry;
@synthesize deleteButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataAry=[[NSMutableArray alloc]init];
    
    self.noticeTab.layer.borderWidth=0.45;
    self.noticeTab.layer.borderColor=[[UIColor colorWithRed:46.0/255 green:138.0/255 blue:208.0/255 alpha:1.0]CGColor];
//    self.noticeTab.layer.cornerRadius=5.0;
    [self.noticeTab.layer setMasksToBounds:YES];
    
    self.noticeTab.separatorStyle =UITableViewCellSeparatorStyleNone;
    if (isIPhonePlat) {
        if (isIPhone5) {
        CGRect bounds =  [(SCRootViewController *)[[UIApplication sharedApplication] keyWindow].rootViewController coreTableBgView].bounds;
            CGRect frame =self.dataView.frame;
            frame.size =(CGSize){bounds.size.width,bounds.size.height -20};// -20 是距离顶部 20
            self.dataView.frame =frame;
        }
    }
}
-(void)viewWillAppear:(BOOL)animated
{
      self.navigationController.navigationBarHidden=YES;
   // self.view.frame=CGRectMake(0, 64+44, 1024, 768);
    
  //   self.view.bounds=CGRectMake(0, -64, 1024, 768);
    
    [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
        NSLog(@"response======%@",response);
        if ([response isKindOfClass:[NSDictionary class]]) {
            [dataAry removeAllObjects];
            NSArray * ary =[response objectForKey:@"remainList"];
            [dataAry addObjectsFromArray:ary];
            [self.noticeTab reloadData];
        }
    } faileture:^(BOOL faileTure) {
        
    } withType:kgetSleepRemind andWithPeroid:0];

}
-(IBAction)deleteButtonClick:(id)sender
{
    if (dataAry&&dataAry.count>0) {
        
       // NSArray * ary =[NSArray arrayWithObject:[NSNumber numberWithInt:regID]];
        NSMutableArray * ary =[NSMutableArray arrayWithCapacity:0];
        for(NSDictionary * mdic in dataAry){
            
            NSInteger isselect =[[mdic objectForKey:@"isSelect"]integerValue];
            if ( isselect==1) {
                 NSInteger regID =[[mdic objectForKey:@"regId"]integerValue];
                [ary addObject:[NSNumber numberWithInt:regID]];
            }
        }
        
        if (ary.count) {
           DXAlertView *alert1 =  ((DXAlertView *)[[DXAlertView alloc] initWithTitle:@"提示" contentText:@"确定删除吗?" leftButtonTitle:@"删除" rightButtonTitle:@"取消"])  ;
            alert1.tag =100;
            alert1.delegate =self;
            [alert1 showStyle:alert];
            
            
        }else{
            [[[DXAlertView alloc] initWithTitle:@"提示" contentText:@"您没有选择要删除的提醒" leftButtonTitle:nil rightButtonTitle:nil] show];

        }
       
        
    }else{
    
//        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"您没有选择要删除的提醒" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        [alert show];
        [[[DXAlertView alloc] initWithTitle:@"提示" contentText:@"您没有选择要删除的提醒" leftButtonTitle:nil rightButtonTitle:nil] show];
    
    }
}


#pragma mark -alertDelegate
-(void)alertView:(DXAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag ==100) {
        if (buttonIndex==0) {
            // 确认
            NSMutableArray * ary =[NSMutableArray arrayWithCapacity:0];
            for(NSDictionary * mdic in dataAry){
                
                NSInteger isselect =[[mdic objectForKey:@"isSelect"]integerValue];
                if ( isselect==1) {
                    NSInteger regID =[[mdic objectForKey:@"regId"]integerValue];
                    [ary addObject:[NSNumber numberWithInt:regID]];
                }
            }
            
            if (ary.count) {
                
                [SCNetManager deleteSleepRemindSuccess:^(BOOL success, NSDictionary *response) {
                    NSLog(@"%@",response);
                    NSInteger  errorcode=[[response objectForKey:@"errorCode"]integerValue];
                    if (errorcode==-1100) {
//                        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"删除成功" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//                        [alert show];
                        [[[DXAlertView alloc] initWithTitle:@"提示" contentText:@"删除成功" leftButtonTitle:nil rightButtonTitle:nil] show];
                        NSInteger regID ,toid;
                        for (int j=0; j<dataAry.count;j++) {
                            NSDictionary *mdic =dataAry[j];
                            regID =[[mdic objectForKey:@"regId"] integerValue];
                            for (NSNumber *number in ary) {
                                toid =[number integerValue];
                                if (regID ==toid) {
                                    [dataAry removeObject:mdic];
                                    j--;
                                }
                            }
                        }
                        [dataAry removeObjectsInArray:ary];
                        [self.noticeTab reloadData];
   //                     [self viewWillAppear:YES];
                        
                    }else{
//                        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"参数有误,删除失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//                        [alert show];
                        [[[DXAlertView alloc] initWithTitle:@"提示" contentText:@"删除失败" leftButtonTitle:nil rightButtonTitle:nil] show];
                    }
                    
                } faileture:^(BOOL faileTure) {
                    
                    [[[DXAlertView alloc] initWithTitle:@"提示" contentText:@"删除失败" leftButtonTitle:nil rightButtonTitle:nil] show];
                    
                } withAry:ary andCount:ary.count];

            }
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// this func is delete
-(void)selectByAry:(NSArray *)ary
{
    self.deleteAry=[NSMutableArray arrayWithArray:ary];

}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataAry.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier=@"ListCell";
    NoticeCell *cell = (NoticeCell*)(UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        UIViewController *cellcontroller;
        if (isIPhonePlat) {
             cellcontroller =[[UIViewController alloc]initWithNibName:@"NoticeCell_iPhone" bundle:nil];
        }else{
            
            cellcontroller =[[UIViewController alloc]initWithNibName:@"NoticeCell" bundle:nil];
        }
       
        cell =(NoticeCell*)cellcontroller.view;
    }
   
    if (dataAry.count) {
        NSDictionary * mdic =[dataAry objectAtIndex:indexPath.row];
        cell.dataAry=dataAry;
        [cell displayDic:mdic indexNum:indexPath.row];
    }
    cell.delegate=self;
//    [tableView  setSeparatorColor:[UIColor colorWithRed:52.0/255 green:142.0/255 blue:253.0/255 alpha:0.35]];  //设置分割线为蓝色
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * mdic  =[dataAry objectAtIndex:indexPath.row];
    
    SCTiXingDetailVC *detial;
    if (isIPhonePlat) {
        detial =[[SCTiXingDetailVC alloc]initWithNibName:@"SCTiXingDetailVC_iPhone" bundle:nil];
    }else{
        detial =[[SCTiXingDetailVC alloc]initWithNibName:@"SCTiXingDetailVC_iPad" bundle:nil];
    }
    
   
    [detial displayDic:mdic];
    [self.navigationController pushViewController:detial animated:YES];
    

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  UINavigationController+autoRotate.m
//  Furniture_basic
//
//  Created by ziheli on 12-11-26.
//
//

#import "UINavigationController+autoRotate.h"

@implementation UINavigationController (autoRotate)

#ifdef __IPHONE_6_0
-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}
#endif

@end

//
//  SCHistoryVC.m
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCHistoryVC.h"
#import "SCNetManager.h"
#import "SCShareFunc.h"

#import "UIButton+openTapArea.h"

#define SB  @"saveStatus"

#define OW      @"oneweek"
#define TW      @"twoweek"
#define OM      @"onemonth"
#define INDEX   @"index"

@interface SCHistoryVC ()

@end

@implementation SCHistoryVC
@synthesize redPointAry;
@synthesize greePointAry;
@synthesize bluePointAry;
@synthesize pageScrollView;
@synthesize contentView;
@synthesize intruduceImage;
@synthesize TimeAry;
@synthesize periodSleepQuiltyList;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        

        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSMutableArray * _redAry =[[NSMutableArray alloc]init];
    self.redPointAry=_redAry;
    NSMutableArray * _greenAry =[[NSMutableArray alloc]init];
    self.greePointAry=_greenAry;
    NSMutableArray * _blueAry =[[NSMutableArray alloc]init];
    self.bluePointAry=_blueAry;
    
    
    NSMutableArray * _timeAry =[[NSMutableArray alloc]init];
    self.TimeAry=_timeAry;
    
    NSMutableArray *_quiltylist=[[NSMutableArray alloc]init];
    self.periodSleepQuiltyList=_quiltylist;
    
    ary1=[[NSMutableArray alloc]init];
    ary2=[[NSMutableArray alloc]init];
    ary3=[[NSMutableArray alloc]init];
    
        NSMutableDictionary * mdic =[NSMutableDictionary dictionaryWithCapacity:0];
        [mdic setObject:[NSNumber numberWithBool:YES] forKey:@"oneweek"];
        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"twoweek"];
        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"onemonth"];
        [mdic setObject:[NSNumber numberWithInteger:0] forKey:INDEX];
    
        [ary1 addObject:[mdic mutableCopy]];
        [ary2 addObject:[mdic mutableCopy]];
        [ary3 addObject:[mdic mutableCopy]];
    
    self.view.userInteractionEnabled=YES;
    contentView.userInteractionEnabled=YES;
    //self.photos=[NSMutableArray arrayWithObjects:nil];
    NSInteger knumbers =3;

    views =[[NSMutableArray alloc] initWithCapacity:knumbers];

    bgView.layer.borderWidth=0.5;
    bgView.layer.borderColor=[[UIColor colorWithRed:46.0/255 green:138.0/255 blue:208.0/255 alpha:1.0]CGColor];
    
    
    [views removeAllObjects];
    for (unsigned i = 0; i < knumbers; i++) {
        [views addObject:[NSNull null]];
    }
    bigScroll.canCancelContentTouches=NO;
    bigScroll.pagingEnabled = YES;
    bigScroll.contentSize = CGSizeMake(bigScroll.frame.size.width*knumbers, bigScroll.frame.size.height);
    bigScroll.showsHorizontalScrollIndicator = NO;
    bigScroll.showsVerticalScrollIndicator = NO;
    bigScroll.scrollsToTop = NO;
    bigScroll.delegate = self;
    bigScroll.bounces=NO;
    
    pageScrollView.delegate=self;
    pageScrollView.pagingEnabled=YES;
    pageScrollView.canCancelContentTouches=NO;
    pageScrollView.showsHorizontalScrollIndicator=NO;
    pageScrollView.showsVerticalScrollIndicator = NO;
    pageScrollView.scrollsToTop = NO;
    pageScrollView.bounces=NO;
    
    CGRect frame = bigScroll.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [bigScroll scrollRectToVisible:frame animated:NO];
    bujialabel.hidden=YES;
    shangkelabel.hidden=YES;
    youlianglabel.hidden=YES;
    
    bujialabel.backgroundColor=[UIColor colorWithRed:203.0/255 green:8.0/255 blue:39.0/255 alpha:1];
    shangkelabel.backgroundColor=[UIColor colorWithRed:248.0/255 green:210.0/255 blue:40.0/255 alpha:1.0];
    youlianglabel.backgroundColor=[UIColor colorWithRed:81.0/255 green:162.0/255 blue:51.0/255 alpha:1.0];
    
    
    isLife=YES;
    isOneWeek=YES;
    isTwoWeek=NO;
    isOneMonth=NO;
    
}


-(void)viewWillAppear:(BOOL)animated
{
//    
//    [self->liftButton  addObserver:self forKeyPath:@"backgroundImage" options:NSKeyValueObservingOptionNew context:nil];
//    [self->sleepButton  addObserver:self forKeyPath:@"backgroundImage" options:NSKeyValueObservingOptionNew context:nil];
//    [self->sleepQuButton  addObserver:self forKeyPath:@"backgroundImage" options:NSKeyValueObservingOptionNew context:nil];
//    [self->oneWeekButton  addObserver:self forKeyPath:@"backgroundImage" options:NSKeyValueObservingOptionNew context:nil];
//    [self->twoWeekButton  addObserver:self forKeyPath:@"backgroundImage" options:NSKeyValueObservingOptionNew context:nil];
//    [self->oneMonth   addObserver:self forKeyPath:@"backgroundImage" options:NSKeyValueObservingOptionNew context:nil];
//    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveStatus:) name:@"saveStatus" object:nil];

    
    
    NSString *type =nil;
    int peroid =0;
    
    if (isLife) {
        type =kgetPeriodBodyStatus;
        if ([[ary1[0] objectForKey:OW] boolValue]) {
            peroid =7;
        }else if ([[ary1[0] objectForKey:TW] boolValue]){
            peroid =14;
        }else if ([[ary1[0] objectForKey:OM] boolValue]){
            peroid =28;
        }
        
        [self lifeButtonClick:nil];
        
    }else if (isSleep){
        type =kgetPeriodInBed;
        if ([[ary2[0] objectForKey:OW] boolValue]) {
            peroid =7;
        }else if ([[ary2[0] objectForKey:TW] boolValue]){
            peroid =14;
        }else if ([[ary2[0] objectForKey:OM] boolValue]){
            peroid =28;
        }
        [self sleepButtonClick:nil];
    }else if (isSleepQua){
        type =kgetPeriodSleepQuilty;
        if ([[ary3[0] objectForKey:OW] boolValue]) {
            peroid =7;
        }else if ([[ary3[0] objectForKey:TW] boolValue]){
            peroid =14;
        }else if ([[ary3[0] objectForKey:OM] boolValue]){
            peroid =28;
        }
        [self sleepQuationButtonClick:nil];
    }
    
    
    
//    [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
//        NSLog(@"lishiresponse====%@",response);
//        NSArray * ary =[response objectForKey:@"bodyStatusList"];
//        NSLog(@"ary=====%@",ary);
//        [self.redPointAry removeAllObjects];
//        [self.greePointAry removeAllObjects];
//        [self.bluePointAry removeAllObjects];
//        for(NSDictionary * mdic in ary){
//            NSInteger avgHeartbert=[[mdic objectForKey:@"avgHeartbert"]integerValue];
//            NSInteger avgBreath=[[mdic objectForKey:@"avgBreath"]integerValue];
//            NSInteger apneaTime=[[mdic objectForKey:@"apneaTime"]integerValue];
//            [self.redPointAry addObject:[NSNumber numberWithInteger:avgHeartbert]];
//            [self.bluePointAry addObject:[NSNumber numberWithInteger:avgBreath]];
//            [self.greePointAry addObject:[NSNumber numberWithInteger:apneaTime]];
//        }
//        
//        NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
//        NSMutableArray * timeAry =[self get7DaysTimeAryBy:str byDate:7];
//        NSLog(@"timeAry=====%@",timeAry);
//        
//        [contentView displayViewByType:1 redPointAry:self.redPointAry greenPointAry:self.greePointAry bluePointAry:self.bluePointAry dateAry:timeAry];
//        
//    } faileture:^(BOOL faileTure) {
//        
//    } withType:kgetPeriodBodyStatus andWithPeroid:7];
    
    

    
}

-(void)saveStatus:(NSNotification *)noti{
    UIButton *btn =noti.object;
    [self cleanStatusOfAll];
    if (isLife) {
        if (btn ==self->oneWeekButton) {
            [ary1[0]  setObject:[NSNumber numberWithBool:YES] forKey:OW];
        }else if (btn ==self->twoWeekButton){
            [ary1[0]  setObject:[NSNumber numberWithBool:YES] forKey:TW];
        }else if (btn ==self->oneMonth){
            [ary1[0] setObject:[NSNumber numberWithBool:YES] forKey:OM];
        }
        [ary1[0] setObject:[NSNumber numberWithInteger:0] forKey:INDEX];
        
    }else if (isSleep){
    
        if (btn ==self->oneWeekButton) {
            [ary2[0]  setObject:[NSNumber numberWithBool:YES] forKey:OW];
        }else if (btn ==self->twoWeekButton){
            [ary2[0]  setObject:[NSNumber numberWithBool:YES] forKey:TW];
        }else if (btn ==self->oneMonth){
            [ary2[0] setObject:[NSNumber numberWithBool:YES] forKey:OM];
        }
        [ary2[0] setObject:[NSNumber numberWithInteger:0] forKey:INDEX];
    }else if (isSleepQua){
    
        if (btn ==self->oneWeekButton) {
            [ary3[0]  setObject:[NSNumber numberWithBool:YES] forKey:OW];
        }else if (btn ==self->twoWeekButton){
            [ary3[0]  setObject:[NSNumber numberWithBool:YES] forKey:TW];
        }else if (btn ==self->oneMonth){
            [ary3[0] setObject:[NSNumber numberWithBool:YES] forKey:OM];
        }
        
        [ary3[0] setObject:[NSNumber numberWithInteger:0] forKey:INDEX];
    }
    
}

-(void)cleanStatusOfAll{
    if (isLife) {
        [ary1[0] setObject:[NSNumber numberWithBool:NO] forKey:OW];
        [ary1[0] setObject:[NSNumber numberWithBool:NO] forKey:TW];
        [ary1[0] setObject:[NSNumber numberWithBool:NO] forKey:OM];
    }else if (isSleep){
    
        [ary2[0] setObject:[NSNumber numberWithBool:NO] forKey:OW];
        [ary2[0] setObject:[NSNumber numberWithBool:NO] forKey:TW];
        [ary2[0] setObject:[NSNumber numberWithBool:NO] forKey:OM];
    }else if (isSleepQua){
    
        [ary3[0] setObject:[NSNumber numberWithBool:NO] forKey:OW];
        [ary3[0] setObject:[NSNumber numberWithBool:NO] forKey:TW];
        [ary3[0] setObject:[NSNumber numberWithBool:NO] forKey:OM];
    }else{
    
        NSLog(@"not current");
        return;
    }
    
}

-(void)setBackGroudImageUseLiftButtonSelect:(BOOL)yesLift sleepSelect:(BOOL)yesSleep qulSelcet:(BOOL)yesQua{
    UIImage *image;
    if (isIPhonePlat) {
        if (yesLift) {
            image =[UIImage imageNamed:@"_4shengmingtizheng-lan"];
        }else{
            image =[UIImage imageNamed:@"_4shengmingtizheng-bai"];
        }
        [liftButton  setBackgroundImage:image forState:UIControlStateNormal];
        if (yesSleep) {
            image =[UIImage imageNamed:@"_4shuimianzhuangtai-lan"];
        }else{
             image =[UIImage imageNamed:@"_4shuimianzhuangtai-bai"];
        }
        [sleepButton setBackgroundImage:image forState:UIControlStateNormal];
        
        if (yesQua) {
            image =[UIImage imageNamed:@"_4shuimianzhiliang-lan"];
        }else{
            image =[UIImage imageNamed:@"_4shuimianzhiliang-bai"];
        }
        [sleepQuButton setBackgroundImage:image forState:UIControlStateNormal];
    }else{
        if (yesLift) {
            image =[UIImage imageNamed:@"shengmingtizheng-xuanzhong"];
        }else{
            image =[UIImage imageNamed:@"shengmingtizheng-moren"];
        }
        [liftButton setBackgroundImage:image forState:UIControlStateNormal];
        
        if (yesSleep) {
             image =[UIImage imageNamed:@"shuimianzhuangtai-xuanzhong"];
        }else{
             image =[UIImage imageNamed:@"shuimianzhuangtai-moren"];
        }
        [sleepButton setBackgroundImage:image forState:UIControlStateNormal];
        
        if (yesQua) {
            image =[UIImage imageNamed:@"shuimianzhiliang-xuanzhong"];
   
        }else{
            image =[UIImage imageNamed:@"shuimianzhiliang-moren"];
        }
        [sleepQuButton setBackgroundImage:image forState:UIControlStateNormal];
    }
}

-(IBAction)lifeButtonClick:(UIButton*)button
{
    
   

    isLife=YES;
    isSleep=NO;
    isSleepQua=NO;
    intruduceImage.hidden=NO;
    bujialabel.hidden=YES;
    shangkelabel.hidden=YES;
    youlianglabel.hidden=YES;
    
    [self setBackGroudImageUseLiftButtonSelect:YES sleepSelect:NO qulSelcet:NO];
    
//    [liftButton setBackgroundImage:[UIImage imageNamed:@"shengmingtizheng-xuanzhong"] forState:UIControlStateNormal];
//    [sleepButton setBackgroundImage:[UIImage imageNamed:@"shuimianzhuangtai-moren"] forState:UIControlStateNormal];
//    [sleepQuButton setBackgroundImage:[UIImage imageNamed:@"shuimianzhiliang-moren"] forState:UIControlStateNormal];
//    NSMutableDictionary * mdic=[ary1 objectAtIndex:0];
//    BOOL oneweek =[[mdic objectForKey:@"oneweek"]boolValue];
//    BOOL twoweek =[[mdic objectForKey:@"twoweek"]boolValue];
//    BOOL onemonth =[[mdic objectForKey:@"onemonth"]boolValue];
//    if (oneweek) {
//        isOneWeek=YES;
//      [self oneWeekButtonClick:nil];
//    }else if (twoweek){
//        isTwoWeek=YES;
//      [self twoWeekButtonClick:nil];
//    }else if (onemonth){
//        isOneMonth=YES;
//      [self oneWeekButtonClick:nil];
//    }

    
    [oneWeekButton setBackgroundImage:  ([[ary1[0] objectForKey:OW] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    [twoWeekButton setBackgroundImage:([[ary1[0] objectForKey:TW] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"]forState:0];
    [oneMonth setBackgroundImage:([[ary1[0] objectForKey:OM] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    
    // 设置选中
    bigScroll.contentOffset =(CGPoint){0,0};
    autoView.frame =(CGRect){0,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
    
//    contentView.backgroundColor =[UIColor orangeColor];
//    pageScrollView.backgroundColor =[UIColor redColor];
//    NSLog(@"%@,pagescroll %@",NSStringFromCGRect(autoView.frame),NSStringFromCGRect(pageScrollView.frame));
    
    //from 1 to n
    int numCount  =([[ary1[0] objectForKey:OW] boolValue])?1:(([[ary1[0] objectForKey:TW] boolValue])?2:(([[ary1[0] objectForKey:OM] boolValue])?4:0));
    pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
    
    // index pagecrollview ,from 0 to n-1
    int index =[[ary1[0] objectForKey:INDEX] integerValue];
    pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
    
    
    contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
    
    
    
    NSString *type =nil;
    int peroid =0;
    
    if (isLife) {
        type =kgetPeriodBodyStatus;
        if ([[ary1[0] objectForKey:OW] boolValue]) {
            peroid =7;
        }else if ([[ary1[0] objectForKey:TW] boolValue]){
            peroid =14;
        }else if ([[ary1[0] objectForKey:OM] boolValue]){
            peroid =28;
        }
    }
    
    [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
        NSLog(@"lishiresponse====%@",response);
        NSArray * ary =[response objectForKey:@"bodyStatusList"];
        NSLog(@"ary=====%@",ary);
        [self.redPointAry removeAllObjects];
        [self.greePointAry removeAllObjects];
        [self.bluePointAry removeAllObjects];
        for(NSDictionary * mdic in ary){
            NSInteger avgHeartbert=[[mdic objectForKey:@"avgHeartbert"]integerValue];
            NSInteger avgBreath=[[mdic objectForKey:@"avgBreath"]integerValue];
            NSInteger apneaTime=[[mdic objectForKey:@"apneaTime"]integerValue];
            [self.redPointAry addObject:[NSNumber numberWithInteger:avgHeartbert]];
            [self.bluePointAry addObject:[NSNumber numberWithInteger:avgBreath]];
            [self.greePointAry addObject:[NSNumber numberWithInteger:apneaTime]];
        }
        
//        NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
////        NSMutableArray * timeAry =[self get7DaysTimeAryBy:str byDate:peroid];
        
        [self lazyChangeScro:pageScrollView];
//        [contentView displayViewByType:1 redPointAry:self.redPointAry greenPointAry:self.greePointAry bluePointAry:self.bluePointAry dateAry:timeAry];
//        [bigScroll setContentOffset:CGPointMake(0, 0)];
//        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
//        autoView.frame=CGRectMake(0, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);

        NSLog(@"fsfsdfds");
        
    } faileture:^(BOOL faileTure) {
        
         [self lazyChangeScro:pageScrollView];
        
    } withType:type andWithPeroid:peroid];

    
}
-(IBAction)sleepButtonClick:(UIButton*)button
{
    isLife=NO;
    isSleep=YES;
    isSleepQua=NO;
    
    bujialabel.hidden=YES;
    shangkelabel.hidden=YES;
    youlianglabel.hidden=YES;
    
//    NSMutableDictionary * mdic=[ary2 objectAtIndex:0];
//    BOOL oneweek =[[mdic objectForKey:@"oneweek"]boolValue];
//    BOOL twoweek =[[mdic objectForKey:@"twoweek"]boolValue];
//    BOOL onemonth =[[mdic objectForKey:@"onemonth"]boolValue];
//    
//    if (oneweek) {
//        isOneWeek=YES;
//        [self oneWeekButtonClick:nil];
//    }else if (twoweek){
//        isTwoWeek=YES;
//        [self twoWeekButtonClick:nil];
//    }else if (onemonth){
//        isOneMonth=YES;
//        [self oneWeekButtonClick:nil];
//    }
    
    
//    
    NSString *type =nil;
    int peroid =0;
    if (isSleep){
        type =kgetPeriodInBed;
        if ([[ary2[0] objectForKey:OW] boolValue]) {
            peroid =7;
        }else if ([[ary2[0] objectForKey:TW] boolValue]){
            peroid =14;
        }else if ([[ary2[0] objectForKey:OM] boolValue]){
            peroid =28;
        }
    }
    
    [self setBackGroudImageUseLiftButtonSelect:NO sleepSelect:YES qulSelcet:NO];
    
//    [liftButton setBackgroundImage:[UIImage imageNamed:@"shengmingtizheng-moren"] forState:UIControlStateNormal];
//    [sleepButton setBackgroundImage:[UIImage imageNamed:@"shuimianzhuangtai-xuanzhong"] forState:UIControlStateNormal];
//    [sleepQuButton setBackgroundImage:[UIImage imageNamed:@"shuimianzhiliang-moren"] forState:UIControlStateNormal];
//    
    
    [oneWeekButton setBackgroundImage:  ([[ary2[0] objectForKey:OW] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    [twoWeekButton setBackgroundImage:([[ary2[0] objectForKey:TW] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"]forState:0];
    [oneMonth setBackgroundImage:([[ary2[0] objectForKey:OM] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    
    
    
    bigScroll.contentOffset =(CGPoint){bigScroll.bounds.size.width*1,0};
    autoView.frame =(CGRect){bigScroll.bounds.size.width*1,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
    
    //from 1 to n
    int numCount  =1;
    pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
    
    // index pagecrollview ,from 0 to n-1
    int index =0;
    pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
    
    
    contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};

    
    
  
    [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
        
        NSLog(@"lishiresponse====%@",response);
        [self.TimeAry removeAllObjects];
       self.peroidInBedList =[response objectForKey:@"peroidInBedList"];
        [self.TimeAry addObjectsFromArray:_peroidInBedList];
        NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
       self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:peroid];
        intruduceImage.hidden=YES;
//        [bigScroll setContentOffset:CGPointMake(bigScroll.frame.size.width*1, 0)];
//        
//       [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
//        autoView.frame=CGRectMake(bigScroll.frame.size.width*1, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);
//        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
         [self lazyChangeScro:pageScrollView];
       // [contentView displayperoidInBedListBy:peroidInBedList dateAry:timeAry];
        
    } faileture:^(BOOL faileTure) {
        intruduceImage.hidden=YES;
         [self lazyChangeScro:pageScrollView];
        
    } withType:type andWithPeroid:peroid];

    
    
}
-(IBAction)sleepQuationButtonClick:(UIButton*)button
{
    isLife=NO;
    isSleep=NO;
    isSleepQua=YES;
    intruduceImage.hidden=YES;
    
    bujialabel.hidden=NO;
    shangkelabel.hidden=NO;
    youlianglabel.hidden=NO;
    
    
    
//    NSMutableDictionary * mdic=[ary3 objectAtIndex:0];
//    BOOL oneweek =[[mdic objectForKey:@"oneweek"]boolValue];
//    BOOL twoweek =[[mdic objectForKey:@"twoweek"]boolValue];
//    BOOL onemonth =[[mdic objectForKey:@"onemonth"]boolValue];
//    if (oneweek) {
//        isOneWeek=YES;
//        [self oneWeekButtonClick:nil];
//    }else if (twoweek){
//        isTwoWeek=YES;
//        [self twoWeekButtonClick:nil];
//    }else if (onemonth){
//        isOneMonth=YES;
//        [self oneWeekButtonClick:nil];
//    }
    
    [self setBackGroudImageUseLiftButtonSelect:NO sleepSelect:NO qulSelcet:YES];
//    [liftButton setBackgroundImage:[UIImage imageNamed:@"shengmingtizheng-moren"] forState:UIControlStateNormal];
//    [sleepButton setBackgroundImage:[UIImage imageNamed:@"shuimianzhuangtai-moren"] forState:UIControlStateNormal];
//    [sleepQuButton setBackgroundImage:[UIImage imageNamed:@"shuimianzhiliang-xuanzhong"] forState:UIControlStateNormal];
    
    
    [oneWeekButton setBackgroundImage:  ([[ary3[0] objectForKey:OW] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    [twoWeekButton setBackgroundImage:([[ary3[0] objectForKey:TW] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"]forState:0];
    [oneMonth setBackgroundImage:([[ary3[0] objectForKey:OM] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    
    // 设置选中
    bigScroll.contentOffset =(CGPoint){bigScroll.bounds.size.width*2,0};
    autoView.frame =(CGRect){bigScroll.bounds.size.width*2,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
    
    //    contentView.backgroundColor =[UIColor orangeColor];
    //    pageScrollView.backgroundColor =[UIColor redColor];
    //    NSLog(@"%@,pagescroll %@",NSStringFromCGRect(autoView.frame),NSStringFromCGRect(pageScrollView.frame));
    
    //from 1 to n
    int numCount  =([[ary3[0] objectForKey:OW] boolValue])?1:(([[ary3[0] objectForKey:TW] boolValue])?2:(([[ary3[0] objectForKey:OM] boolValue])?4:0));
    pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
    
    // index pagecrollview ,from 0 to n-1
    int index =[[ary3[0] objectForKey:INDEX] integerValue];
    pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
    
    
    contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
    
    
    
    NSString *type =nil;
    int peroid =0;
    
    if (isSleepQua) {
        type =kgetPeriodSleepQuilty;
        if ([[ary3[0] objectForKey:OW] boolValue]) {
            peroid =7;
        }else if ([[ary3[0] objectForKey:TW] boolValue]){
            peroid =14;
        }else if ([[ary3[0] objectForKey:OM] boolValue]){
            peroid =28;
        }
    }

    [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
        
        NSLog(@"lishiresponse====%@",response);
        [self.periodSleepQuiltyList removeAllObjects];
        NSArray * qulist=[response objectForKey:@"periodSleepQuiltyList"];
        [self.periodSleepQuiltyList addObjectsFromArray:qulist];
        
        NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
       self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:peroid];
        
        [self lazyChangeScro:pageScrollView];
        
//        [contentView displayperoidSleepQuiltyList:self.periodSleepQuiltyList dateAry:timeAry];
//        [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
//        [pageScrollView setContentOffset:CGPointMake(0, 0)];
//        [bigScroll setContentOffset:CGPointMake(bigScroll.frame.size.width*2, 0)];
//        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
//        autoView.frame=CGRectMake(bigScroll.frame.size.width*2, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);
        
    } faileture:^(BOOL faileTure) {
         [self lazyChangeScro:pageScrollView];
        
    } withType:kgetPeriodSleepQuilty andWithPeroid:peroid];
    
    
}

-(IBAction)oneWeekButtonClick:(UIButton*)button
{

//    if (isLife) {
//        NSMutableDictionary * mdic=[ary1 objectAtIndex:0];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"onemonth"];
//        [mdic setObject:[NSNumber numberWithBool:YES] forKey:@"oneweek"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"twoweek"];
//    }else if (isSleep){
//        NSMutableDictionary * mdic=[ary2 objectAtIndex:0];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"onemonth"];
//        [mdic setObject:[NSNumber numberWithBool:YES] forKey:@"oneweek"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"twoweek"];
//    }else if (isSleepQua){
//        NSMutableDictionary * mdic=[ary3 objectAtIndex:0];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"onemonth"];
//        [mdic setObject:[NSNumber numberWithBool:YES] forKey:@"oneweek"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"twoweek"];
//    }
    
    
    
    if ((isOneMonth&& isSleepQua)||(isTwoWeek&& isSleepQua)) {
        [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
        [pageScrollView setContentOffset:CGPointMake(0, 0)];
        [bigScroll setContentOffset:CGPointMake(bigScroll.frame.size.width*2, 0)];
        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
        autoView.frame=CGRectMake(bigScroll.frame.size.width*2, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);
    }else if ((isOneMonth&& isSleep)||(isTwoWeek&& isSleep)){
    
        [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
        [pageScrollView setContentOffset:CGPointMake(0, 0)];
        [bigScroll setContentOffset:CGPointMake(bigScroll.frame.size.width*1, 0)];
        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
        autoView.frame=CGRectMake(bigScroll.frame.size.width*1, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);
    
    }else if ((isOneMonth&& isLife)||(isTwoWeek&& isLife)){
    
        [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
        [pageScrollView setContentOffset:CGPointMake(0, 0)];
        [bigScroll setContentOffset:CGPointMake(0, 0)];
        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
        autoView.frame=CGRectMake(0, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);
    }
    
    [oneWeekButton setBackgroundImage:[UIImage imageNamed:@"xuanzhong-zhou"] forState:0];
    [twoWeekButton setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    [oneMonth setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    
    isOneWeek=YES;
    isTwoWeek=NO;
    isOneMonth=NO;
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SB object:button];
    
    
    if (isLife) {
        
        // 设置选中
        bigScroll.contentOffset =(CGPoint){0,0};
        autoView.frame =(CGRect){0,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
        
        //from 1 to n
        int numCount  =([[ary1[0] objectForKey:OW] boolValue])?1:(([[ary1[0] objectForKey:TW] boolValue])?2:(([[ary1[0] objectForKey:OM] boolValue])?4:0));
        pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
        
        // index pagecrollview ,from 0 to n-1
        int index =[[ary1[0] objectForKey:INDEX] integerValue];
        pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
        
        
        contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
        
        
        
        
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            
            NSArray * ary =[response objectForKey:@"bodyStatusList"];
            NSLog(@"ary=====%@",ary);
            [self.redPointAry removeAllObjects];
            [self.greePointAry removeAllObjects];
            [self.bluePointAry removeAllObjects];
            for(NSDictionary * mdic in ary){
                NSInteger avgHeartbert=[[mdic objectForKey:@"avgHeartbert"]integerValue];
                NSInteger avgBreath=[[mdic objectForKey:@"avgBreath"]integerValue];
                NSInteger apneaTime=[[mdic objectForKey:@"apneaTime"]integerValue];
                [self.redPointAry addObject:[NSNumber numberWithInteger:avgHeartbert]];
                [self.bluePointAry addObject:[NSNumber numberWithInteger:avgBreath]];
                [self.greePointAry addObject:[NSNumber numberWithInteger:apneaTime]];
            }
            //dateStringToMonthDayFromDate
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
            self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:7];
            [self lazyChangeScro:pageScrollView];
//            [contentView displayViewByType:1 redPointAry:self.redPointAry greenPointAry:self.greePointAry bluePointAry:self.bluePointAry dateAry:timeAry];
//            [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
        } faileture:^(BOOL faileTure) {
             [self lazyChangeScro:pageScrollView];
        } withType:kgetPeriodBodyStatus andWithPeroid:7];
        
    }else if (isSleep){
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            
            [self.TimeAry removeAllObjects];
           self.peroidInBedList=[response objectForKey:@"peroidInBedList"];
            [self.TimeAry addObjectsFromArray:self.peroidInBedList];
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
           self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:7];
            
            intruduceImage.hidden=YES;
            [self lazyChangeScro:pageScrollView];
//            [contentView displayViewByType:2 redPointAry:self.redPointAry greenPointAry:self.greePointAry bluePointAry:self.bluePointAry dateAry:timeAry];
//            
//            [contentView displayperoidInBedListBy:peroidInBedList dateAry:timeAry];
//            
//              [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
            
        } faileture:^(BOOL faileTure) {
             [self lazyChangeScro:pageScrollView];
            
        } withType:kgetPeriodInBed andWithPeroid:7];
        
    }else if (isSleepQua){
        
        // 设置选中
        bigScroll.contentOffset =(CGPoint){bigScroll.bounds.size.width*2,0};
        autoView.frame =(CGRect){bigScroll.bounds.size.width*2,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
        
        //from 1 to n
        int numCount  =([[ary3[0] objectForKey:OW] boolValue])?1:(([[ary3[0] objectForKey:TW] boolValue])?2:(([[ary3[0] objectForKey:OM] boolValue])?4:0));
        pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
        
        // index pagecrollview ,from 0 to n-1
        int index =[[ary3[0] objectForKey:INDEX] integerValue];
        pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
        
        
        contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
        
    
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            
            NSLog(@"lishiresponse====%@",response);
            NSLog(@"lishiresponse====%@",response);
            [self.periodSleepQuiltyList removeAllObjects];
            NSArray * qulist=[response objectForKey:@"periodSleepQuiltyList"];
            [self.periodSleepQuiltyList addObjectsFromArray:qulist];
            
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
            self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:7];
            
            [self lazyChangeScro:pageScrollView];
//            [contentView displayperoidSleepQuiltyList:self.periodSleepQuiltyList dateAry:timeAry];
//
//              [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
           
        } faileture:^(BOOL faileTure) {
            
             [self lazyChangeScro:pageScrollView];
            
        } withType:kgetPeriodSleepQuilty andWithPeroid:7];
    }
}
-(IBAction)twoWeekButtonClick:(UIButton*)button
{
    

    
    
    if (isOneMonth&& isSleepQua) {
        [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
        [pageScrollView setContentOffset:CGPointMake(0, 0)];
        [bigScroll setContentOffset:CGPointMake(bigScroll.frame.size.width*2, 0)];
        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
        autoView.frame=CGRectMake(bigScroll.frame.size.width*2, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);
    }else if (isOneMonth&&isSleep){
        [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
        [pageScrollView setContentOffset:CGPointMake(0, 0)];
        [bigScroll setContentOffset:CGPointMake(bigScroll.frame.size.width*1, 0)];
        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
        autoView.frame=CGRectMake(bigScroll.frame.size.width*1, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);
    
    }else if (isOneMonth&&isLife){
        [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
        [pageScrollView setContentOffset:CGPointMake(0, 0)];
        [bigScroll setContentOffset:CGPointMake(0, 0)];
        contentView.frame=CGRectMake(0, contentView.frame.origin.y, contentView.frame.size.width, contentView.frame.size.height);
        autoView.frame=CGRectMake(0, autoView.frame.origin.y, autoView.frame.size.width, autoView.frame.size.height);
    }
    
    isOneWeek=NO;
    isTwoWeek=YES;
    isOneMonth=NO;
    [oneWeekButton setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    [twoWeekButton setBackgroundImage:[UIImage imageNamed:@"xuanzhong-zhou"] forState:0];
    [oneMonth setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SB object:button];
    
    
    if (isLife) {
        // 设置选中
        bigScroll.contentOffset =(CGPoint){0,0};
        autoView.frame =(CGRect){0,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
        
        //from 1 to n
        int numCount  =([[ary1[0] objectForKey:OW] boolValue])?1:(([[ary1[0] objectForKey:TW] boolValue])?2:(([[ary1[0] objectForKey:OM] boolValue])?4:0));
        pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
        
        // index pagecrollview ,from 0 to n-1
        int index =[[ary1[0] objectForKey:INDEX] integerValue];
        pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
        
        
        contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
        
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            NSArray * ary =[response objectForKey:@"bodyStatusList"];
            NSLog(@"ary=====%@",ary);
            [self.redPointAry removeAllObjects];
            [self.greePointAry removeAllObjects];
            [self.bluePointAry removeAllObjects];
            for(NSDictionary * mdic in ary){
                NSInteger avgHeartbert=[[mdic objectForKey:@"avgHeartbert"]integerValue];
                NSInteger avgBreath=[[mdic objectForKey:@"avgBreath"]integerValue];
                NSInteger apneaTime=[[mdic objectForKey:@"apneaTime"]integerValue];
                [self.redPointAry addObject:[NSNumber numberWithInteger:avgHeartbert]];
                [self.bluePointAry addObject:[NSNumber numberWithInteger:avgBreath]];
                [self.greePointAry addObject:[NSNumber numberWithInteger:apneaTime]];
            }
            
            NSLog(@"heartAry====%@",self.redPointAry);
            NSLog(@"breathAry====%@",self.bluePointAry);
            NSLog(@"apneaAry====%@",self.greePointAry);
            
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
           self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:14];
            
            [self lazyChangeScro:pageScrollView];
//            [contentView displayViewByType:1 redPointAry:self.redPointAry greenPointAry:self.greePointAry bluePointAry:self.bluePointAry dateAry:timeAry];
//            [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width*2, pageScrollView.frame.size.height)];
            
        } faileture:^(BOOL faileTure) {
             [self lazyChangeScro:pageScrollView];
            
        } withType:kgetPeriodBodyStatus andWithPeroid:14];
    }else if (isSleep){
        
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            
            [self.TimeAry removeAllObjects];
           self.peroidInBedList=[response objectForKey:@"peroidInBedList"];
            [self.TimeAry addObjectsFromArray:self.peroidInBedList];
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
           self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:14];
            intruduceImage.hidden=YES;
            [self lazyChangeScro:pageScrollView];
//            [contentView displayperoidInBedListBy:peroidInBedList dateAry:timeAry];
//             [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width, pageScrollView.frame.size.height)];
            
        } faileture:^(BOOL faileTure) {
             [self lazyChangeScro:pageScrollView];
            
        } withType:kgetPeriodInBed andWithPeroid:14];
    }else if (isSleepQua){
        
        // 设置选中
        bigScroll.contentOffset =(CGPoint){bigScroll.bounds.size.width*2,0};
        autoView.frame =(CGRect){bigScroll.bounds.size.width*2,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
        
        //from 1 to n
        int numCount  =([[ary3[0] objectForKey:OW] boolValue])?1:(([[ary3[0] objectForKey:TW] boolValue])?2:(([[ary3[0] objectForKey:OM] boolValue])?4:0));
        pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
        
        // index pagecrollview ,from 0 to n-1
        int index =[[ary3[0] objectForKey:INDEX] integerValue];
        pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
        
        
        contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
        
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            
            [self.periodSleepQuiltyList removeAllObjects];
            NSArray * qulist=[response objectForKey:@"periodSleepQuiltyList"];
            [self.periodSleepQuiltyList addObjectsFromArray:qulist];
            
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
           self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:7];
            
            [self lazyChangeScro:pageScrollView];
//            [contentView displayperoidSleepQuiltyList:self.periodSleepQuiltyList dateAry:timeAry];
//            
//            [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width*2, pageScrollView.frame.size.height)];

            
        } faileture:^(BOOL faileTure) {
             [self lazyChangeScro:pageScrollView];
            
        } withType:kgetPeriodSleepQuilty andWithPeroid:14];
        
    }

}
-(IBAction)oneMonthButtonClick:(UIButton*)button
{
//    if (isLife) {
//        NSMutableDictionary * mdic=[ary1 objectAtIndex:0];
//        [mdic setObject:[NSNumber numberWithBool:YES] forKey:@"onemonth"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"oneweek"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"twoweek"];
//    }else if (isSleep){
//        NSMutableDictionary * mdic=[ary2 objectAtIndex:0];
//        [mdic setObject:[NSNumber numberWithBool:YES] forKey:@"onemonth"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"oneweek"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"twoweek"];
//    }else if (isSleepQua){
//        NSMutableDictionary * mdic=[ary3 objectAtIndex:0];
//        [mdic setObject:[NSNumber numberWithBool:YES] forKey:@"onemonth"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"oneweek"];
//        [mdic setObject:[NSNumber numberWithBool:NO] forKey:@"twoweek"];
//    }
    
    
    isOneWeek=NO;
    isTwoWeek=NO;
    isOneMonth=YES;
    [oneWeekButton setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    [twoWeekButton setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
    [oneMonth setBackgroundImage:[UIImage imageNamed:@"xuanzhong-zhou"] forState:0];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SB object:button];
    
    if (isLife) {
        // 设置选中
        bigScroll.contentOffset =(CGPoint){0,0};
        autoView.frame =(CGRect){0,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
        
        //from 1 to n
        int numCount  =([[ary1[0] objectForKey:OW] boolValue])?1:(([[ary1[0] objectForKey:TW] boolValue])?2:(([[ary1[0] objectForKey:OM] boolValue])?4:0));
        pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
        
        // index pagecrollview ,from 0 to n-1
        int index =[[ary1[0] objectForKey:INDEX] integerValue];
        pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
        
        
        contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
        
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            NSLog(@"lishiresponse====%@",response);
            NSArray * ary =[response objectForKey:@"bodyStatusList"];
            NSLog(@"ary=====%@",ary);
            [self.redPointAry removeAllObjects];
            [self.greePointAry removeAllObjects];
            [self.bluePointAry removeAllObjects];
            for(NSDictionary * mdic in ary){
                NSInteger avgHeartbert=[[mdic objectForKey:@"avgHeartbert"]integerValue];
                NSInteger avgBreath=[[mdic objectForKey:@"avgBreath"]integerValue];
                NSInteger apneaTime=[[mdic objectForKey:@"apneaTime"]integerValue];
                [self.redPointAry addObject:[NSNumber numberWithInteger:avgHeartbert]];
                [self.bluePointAry addObject:[NSNumber numberWithInteger:avgBreath]];
                [self.greePointAry addObject:[NSNumber numberWithInteger:apneaTime]];
            }
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
           self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:28];
            
            [self lazyChangeScro:pageScrollView];
            
//            [contentView displayViewByType:1 redPointAry:self.redPointAry greenPointAry:self.greePointAry bluePointAry:self.bluePointAry dateAry:timeAry];
//            [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width*4, pageScrollView.frame.size.height)];
            
        } faileture:^(BOOL faileTure) {
            
             [self lazyChangeScro:pageScrollView];
            
        } withType:kgetPeriodBodyStatus andWithPeroid:28];
    }else if (isSleep){
        
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            
            [self.TimeAry removeAllObjects];
            //
           self.peroidInBedList=[response objectForKey:@"peroidInBedList"];
            [self.TimeAry addObjectsFromArray:self.peroidInBedList];
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
            self.MonthDayTimeArr=[self get7DaysTimeAryBy:str byDate:28];
            intruduceImage.hidden=YES;
            [self lazyChangeScro:pageScrollView];
//            [contentView displayperoidInBedListBy:peroidInBedList dateAry:timeAry];
//            
//             [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width*1, pageScrollView.frame.size.height)];
            
        } faileture:^(BOOL faileTure) {
            
             [self lazyChangeScro:pageScrollView];
            
        } withType:kgetPeriodInBed andWithPeroid:28];
    }else if (isSleepQua){
        
        // 设置选中
        bigScroll.contentOffset =(CGPoint){bigScroll.bounds.size.width*2,0};
        autoView.frame =(CGRect){bigScroll.bounds.size.width*2,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
        
        //from 1 to n
        int numCount  =([[ary3[0] objectForKey:OW] boolValue])?1:(([[ary3[0] objectForKey:TW] boolValue])?2:(([[ary3[0] objectForKey:OM] boolValue])?4:0));
        pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
        
        // index pagecrollview ,from 0 to n-1
        int index =0;//[[ary3[0] objectForKey:INDEX] integerValue];
        pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
        
        
        contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
        
        [SCNetManager getSleepActivityInfoSuccess:^(BOOL success, NSDictionary *response) {
            
            NSLog(@"lishiresponse====%@",response);
            [self.periodSleepQuiltyList removeAllObjects];
            NSArray * qulist=[response objectForKey:@"periodSleepQuiltyList"];
            [self.periodSleepQuiltyList addObjectsFromArray:qulist];
            NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
         self.MonthDayTimeArr =[self get7DaysTimeAryBy:str byDate:28];
            [self lazyChangeScro:pageScrollView];
            
//            [contentView displayperoidSleepQuiltyList:self.periodSleepQuiltyList dateAry:timeAry];
//            [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width*4, pageScrollView.frame.size.height)];
        } faileture:^(BOOL faileTure) {
            
             [self lazyChangeScro:pageScrollView];
            
        } withType:kgetPeriodSleepQuilty andWithPeroid:28];
    }
}

#pragma mark --------- scrollview

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidEndDragging");
    [self lazyChangeScro:scrollView];
   }
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSLog(@"scrollViewWillDidEndDragging");
    if (decelerate) {
        
    }else{
        [self lazyChangeScro:scrollView];
    }

}

-(void)lazyChangeScro:(UIScrollView *)scrollView{
    if (scrollView==bigScroll) {
        CGFloat pageWidth = bigScroll.frame.size.width;
        int page = floor((bigScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        curPage=page;
        if (curPage==0) {
            if ( (isLife&&isOneMonth)||(isLife&&isTwoWeek)||(isLife&&isOneWeek)) {
            }else{
                [self lifeButtonClick:nil];
            }
        }else if(curPage==1){
            if ( (isSleep&&isOneMonth)||(isSleep&&isTwoWeek)||(isSleep&&isOneWeek)) {
            }else{
                [self sleepButtonClick:nil];
            }
        }else if (curPage==2){
            NSLog(@"curPage2====%d",curPage);
            if ( (isSleepQua&&isOneMonth)||(isSleepQua&&isTwoWeek)||(isSleepQua&&isOneWeek)) {
            }else{
                [self sleepQuationButtonClick:nil];
            }
        }else{
            [self lifeButtonClick:nil];
            [oneWeekButton setBackgroundImage:[UIImage imageNamed:@"xuanzhong-zhou"] forState:0];
            [twoWeekButton setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
            [oneMonth setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
        }
    }else if (scrollView==pageScrollView){
        
        NSLog(@"didendpageview");
        CGFloat pageWidth = pageScrollView.frame.size.width;
        int page = floor((pageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        curPage=page;
        
        //将当前page 保存为 index from 0 to n-1
        if (isLife) {
            [ary1[0] setObject:[NSNumber numberWithInteger:page] forKey:INDEX];
        }else if (isSleep){
            [ary2[0] setObject:[NSNumber numberWithInteger:page] forKey:INDEX];
        }else if (isSleepQua){
            [ary3[0] setObject:[NSNumber numberWithInteger:page] forKey:INDEX ];
        }
        
        // 根据当前 page 来设置 pre next hidden
        // 获取当前 one two one 三个按钮的当前背景 ， 来设置next pre hidden
        if (curPage<0) {
            return;
        }
        [self setPreOrNextBtnHiddenWithPageScrollPage:curPage];
        
        if (isLife) {
            if ((self.redPointAry==nil)||(self.redPointAry.count==0)) {
                
                [contentView displayViewByType:1 redPointAry:self.redPointAry greenPointAry:self.greePointAry bluePointAry:self.bluePointAry dateAry:nil];
                
            }else   if (self.redPointAry.count==7) {
                // 一周
                CGRect frame = pageScrollView.frame;
                frame.origin.x = frame.size.width * 0;
                frame.origin.y =contentView.frame.origin.y;
                contentView.frame = frame;
                NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                NSMutableArray * timeAry =[self get7DaysTimeAryBy:str byDate:7];
                
                [contentView displayViewByType:1 redPointAry:self.redPointAry greenPointAry:self.greePointAry bluePointAry:self.bluePointAry dateAry:timeAry];
                
                
            }else   if (self.redPointAry.count==14) {
                if (curPage==0) {
                    NSRange range =NSMakeRange(0, 7);
                    NSArray * redAry=[self.redPointAry subarrayWithRange:range];
                    NSArray * greenAry=[self.greePointAry subarrayWithRange:range];
                    NSArray * blueAry=[self.bluePointAry subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * timeAry =[self get7DaysTimeAryBy:str byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    contentView.frame = frame;
                    [contentView displayViewByType:1 redPointAry:redAry greenPointAry:greenAry bluePointAry:blueAry dateAry:timeAry];
                    
                }else if (curPage==1){
                    NSRange range =NSMakeRange(7, 7);
                    NSArray * redAry=[self.redPointAry subarrayWithRange:range];
                    NSArray * greenAry=[self.greePointAry subarrayWithRange:range];
                    NSArray * blueAry=[self.bluePointAry subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    
                    NSMutableArray * tmpAry=[self get7DaysTimeAryBy:str byDate:7];
                    NSString * lastStr=[tmpAry lastObject];
                    lastStr =[self oneDayBeforeDay:lastStr];// gyc change 2014-6-27
                    NSMutableArray * timeAry=[self get7DaysTimeAryBy:lastStr byDate:7];
                    
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width*page;
                    frame.origin.y =contentView.frame.origin.y;
                    contentView.frame = frame;
                    
                    [contentView displayViewByType:1 redPointAry:redAry greenPointAry:greenAry bluePointAry:blueAry dateAry:timeAry];
                }
                
            }else if (self.redPointAry.count==28){
                if (curPage==0) {
                    NSRange range =NSMakeRange(0, 7);
                    NSArray * redAry=[self.redPointAry subarrayWithRange:range];
                    NSArray * greenAry=[self.greePointAry subarrayWithRange:range];
                    NSArray * blueAry=[self.bluePointAry subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * timeAry =[self get7DaysTimeAryBy:str byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    
                    contentView.frame = frame;
                    [contentView displayViewByType:1 redPointAry:redAry greenPointAry:greenAry bluePointAry:blueAry dateAry:timeAry];
                }else if (curPage==1){
                    NSRange range =NSMakeRange(7, 7);
                    NSArray * redAry=[self.redPointAry subarrayWithRange:range];
                    NSArray * greenAry=[self.greePointAry subarrayWithRange:range];
                    NSArray * blueAry=[self.bluePointAry subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * tmpAry =[self get7DaysTimeAryBy:str byDate:7];
                    NSString * lastStr =[tmpAry lastObject];
                     lastStr =[self oneDayBeforeDay:lastStr];// gyc change 2014-6-27
                    NSMutableArray * timeAry=[self get7DaysTimeAryBy:lastStr byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    contentView.frame = frame;
                    [contentView displayViewByType:1 redPointAry:redAry greenPointAry:greenAry bluePointAry:blueAry dateAry:timeAry];
                }else if (curPage==2){
                    NSRange range =NSMakeRange(14, 7);
                    NSArray * redAry=[self.redPointAry subarrayWithRange:range];
                    NSArray * greenAry=[self.greePointAry subarrayWithRange:range];
                    NSArray * blueAry=[self.bluePointAry subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * tmpAry =[self get7DaysTimeAryBy:str byDate:7];
                    NSString * lastStr =[tmpAry lastObject];
                     lastStr =[self oneDayBeforeDay:lastStr];// gyc change 2014-6-27
                    NSMutableArray * tmpAry2=[self get7DaysTimeAryBy:lastStr byDate:7];
                    NSString * laststr2=[tmpAry2 lastObject];
                     laststr2 =[self oneDayBeforeDay:laststr2];// gyc change 2014-6-27
                    NSMutableArray * timeAry=[self get7DaysTimeAryBy:laststr2 byDate:7];
                    
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    
                    contentView.frame = frame;
                    [contentView displayViewByType:1 redPointAry:redAry greenPointAry:greenAry bluePointAry:blueAry dateAry:timeAry];
                    
                }else if (curPage==3){
                    
                    NSRange range =NSMakeRange(21, 7);
                    NSArray * redAry=[self.redPointAry subarrayWithRange:range];
                    NSArray * greenAry=[self.greePointAry subarrayWithRange:range];
                    NSArray * blueAry=[self.bluePointAry subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * tmpAry =[self get7DaysTimeAryBy:str byDate:7];
                    NSString * lastStr =[tmpAry lastObject];
                     lastStr =[self oneDayBeforeDay:lastStr];// gyc change 2014-6-27
                    NSMutableArray * tmpAry2=[self get7DaysTimeAryBy:lastStr byDate:7];
                    NSString * laststr2=[tmpAry2 lastObject];
                     laststr2 =[self oneDayBeforeDay:laststr2];// gyc change 2014-6-27
                    NSMutableArray * tmpAry3=[self get7DaysTimeAryBy:laststr2 byDate:7];
                    NSString * laststr3=[tmpAry3 lastObject];
                     laststr3 =[self oneDayBeforeDay:laststr3];// gyc change 2014-6-27
                    NSMutableArray * timeAry=[self get7DaysTimeAryBy:laststr3 byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    
                    contentView.frame = frame;
                    [contentView displayViewByType:1 redPointAry:redAry greenPointAry:greenAry bluePointAry:blueAry dateAry:timeAry];
                }
            }
        }else if (isSleep){
            
            [self setBackGroudImageUseLiftButtonSelect:NO sleepSelect:YES qulSelcet:NO];
//            [liftButton setBackgroundImage:[UIImage imageNamed:@"shengmingtizheng-moren"] forState:UIControlStateNormal];
//            [sleepButton setBackgroundImage:[UIImage imageNamed:@"shuimianzhuangtai-xuanzhong"] forState:UIControlStateNormal];
//            [sleepQuButton setBackgroundImage:[UIImage imageNamed:@"shuimianzhiliang-moren"] forState:UIControlStateNormal];
            
            
            [oneWeekButton setBackgroundImage:  ([[ary2[0] objectForKey:OW] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
            [twoWeekButton setBackgroundImage:([[ary2[0] objectForKey:TW] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"]forState:0];
            [oneMonth setBackgroundImage:([[ary2[0] objectForKey:OM] boolValue]==YES)?[UIImage imageNamed:@"xuanzhong-zhou"]:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
            
            
            
            bigScroll.contentOffset =(CGPoint){bigScroll.bounds.size.width*1,0};
            autoView.frame =(CGRect){bigScroll.bounds.size.width*1,autoView.frame.origin.y,autoView.frame.size.width,autoView.frame.size.height};
            
            //from 1 to n
            int numCount  =1;
            pageScrollView.contentSize =(CGSize){pageScrollView.bounds.size.width*numCount,pageScrollView.bounds.size.height};
            
            // index pagecrollview ,from 0 to n-1
            int index =0;
            pageScrollView.contentOffset =(CGPoint){pageScrollView.bounds.size.width*index,0};
            
            
            contentView.frame =(CGRect){pageScrollView.bounds.size.width*index,contentView.frame.origin.y,pageScrollView.bounds.size.width,pageScrollView.bounds.size.height};
            
             [contentView displayperoidInBedListBy:self.peroidInBedList   dateAry:self.MonthDayTimeArr];


            
        }else if (isSleepQua){
            
            if (self.periodSleepQuiltyList==nil ||(self.periodSleepQuiltyList.count==0)) {
               [contentView displayperoidSleepQuiltyList:nil dateAry:nil];
                
            }else if (self.periodSleepQuiltyList.count ==7){
                
                CGRect frame = pageScrollView.frame;
                frame.origin.x = frame.size.width * 0;
                frame.origin.y =contentView.frame.origin.y;
                contentView.frame = frame;
                NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                NSMutableArray * timeAry =[self get7DaysTimeAryBy:str byDate:7];
                
                [contentView displayperoidSleepQuiltyList:self.periodSleepQuiltyList dateAry:timeAry];

            
            }else
            if (self.periodSleepQuiltyList.count==14) {
                if (curPage==0) {
                    NSRange range =NSMakeRange(0, 7);
                    NSArray * redAry=[self.periodSleepQuiltyList subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * timeAry =[self get7DaysTimeAryBy:str byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    contentView.frame = frame;
                    [contentView displayperoidSleepQuiltyList:redAry dateAry:timeAry];
                    
                }else if (curPage==1){
                    NSRange range =NSMakeRange(7, 7);
                    NSArray * redAry=[self.periodSleepQuiltyList subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * tmpAry =[self get7DaysTimeAryBy:str byDate:7];
                    NSString * lastStr =[tmpAry lastObject];
                     lastStr =[self oneDayBeforeDay:lastStr];// gyc change 2014-6-27
                    NSMutableArray * timeAry=[self get7DaysTimeAryBy:lastStr byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width*page;
                    frame.origin.y =contentView.frame.origin.y;
                    contentView.frame = frame;
                    
                    [contentView displayperoidSleepQuiltyList:redAry dateAry:timeAry];
                }
                
            }else if (self.periodSleepQuiltyList.count==28){
                if (curPage==0) {
                    NSRange range =NSMakeRange(0, 7);
                    NSArray * redAry=[self.periodSleepQuiltyList subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * timeAry =[self get7DaysTimeAryBy:str byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    
                    contentView.frame = frame;
                    [contentView displayperoidSleepQuiltyList:redAry dateAry:timeAry];
                }else if (curPage==1){
                    NSRange range =NSMakeRange(7, 7);
                    NSArray * redAry=[self.periodSleepQuiltyList subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * tmpAry =[self get7DaysTimeAryBy:str byDate:7];
                    NSString * lastStr =[tmpAry lastObject];
                     lastStr =[self oneDayBeforeDay:lastStr];// gyc change 2014-6-27
                    NSMutableArray * timeAry=[self get7DaysTimeAryBy:lastStr byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    contentView.frame = frame;
                    [contentView displayperoidSleepQuiltyList:redAry dateAry:timeAry];
                }else if (curPage==2){
                    NSRange range =NSMakeRange(14, 7);
                    NSArray * redAry=[self.periodSleepQuiltyList subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * tmpAry =[self get7DaysTimeAryBy:str byDate:7];
                    NSString * lastStr =[tmpAry lastObject];
                     lastStr =[self oneDayBeforeDay:lastStr];// gyc change 2014-6-27
                    NSMutableArray * tmpAry2=[self get7DaysTimeAryBy:lastStr byDate:7];
                    NSString * laststr2=[tmpAry2 lastObject];
                     laststr2 =[self oneDayBeforeDay:laststr2];// gyc change 2014-6-27
                    NSMutableArray * timeAry=[self get7DaysTimeAryBy:laststr2 byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    contentView.frame = frame;
                    [contentView displayperoidSleepQuiltyList:redAry dateAry:timeAry];
                    
                }else if (curPage==3){
                    NSRange range =NSMakeRange(21, 7);
                    NSArray * redAry=[self.periodSleepQuiltyList subarrayWithRange:range];
                    NSString * str =[SCShareFunc dateStringToMonthDayFromDate:[NSDate date]];
                    NSMutableArray * tmpAry =[self get7DaysTimeAryBy:str byDate:7];
                    NSString * lastStr =[tmpAry lastObject];
                     lastStr =[self oneDayBeforeDay:lastStr];// gyc change 2014-6-27
                    NSMutableArray * tmpAry2=[self get7DaysTimeAryBy:lastStr byDate:7];
                    NSString * laststr2=[tmpAry2 lastObject];
                     laststr2 =[self oneDayBeforeDay:laststr2];// gyc change 2014-6-27
                    NSMutableArray * tmpAry3=[self get7DaysTimeAryBy:laststr2 byDate:7];
                    NSString * laststr3=[tmpAry3 lastObject];
                     laststr3 =[self oneDayBeforeDay:laststr3];// gyc change 2014-6-27
                    NSMutableArray * timeAry=[self get7DaysTimeAryBy:laststr3 byDate:7];
                    CGRect frame = pageScrollView.frame;
                    frame.origin.x = frame.size.width * page;
                    frame.origin.y =contentView.frame.origin.y;
                    contentView.frame = frame;
                    [contentView displayperoidSleepQuiltyList:redAry dateAry:timeAry];
                }
            }else{
                
                [self sleepQuationButtonClick:nil];
                [oneWeekButton setBackgroundImage:[UIImage imageNamed:@"xuanzhong-zhou"] forState:0];
                [twoWeekButton setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
                [oneMonth setBackgroundImage:[UIImage imageNamed:@"weixxuanzhong-zhou"] forState:0];
                
            }
            
            
        }
    }


}


-(NSMutableArray*)get7DaysTimeAryBy:(NSString*)str byDate:(NSInteger)date
{
    
    //    NSString * headtime=[str substringToIndex:3];
    //   // NSInteger inter=[[str substringToIndex:2]integerValue];
    NSMutableArray * timeAry =[NSMutableArray arrayWithCapacity:0];
    //    NSRange range =NSMakeRange(3, 2);
    //    NSInteger subTime =[[str substringWithRange:range]intValue];
    //    for(int i=subTime-date;i<=subTime;i++){
    //        NSLog(@"subTime=============%d",i);
    //        NSString * substr =[NSString stringWithFormat:@"%@%d日",headtime,i];
    //        [timeAry insertObject:substr atIndex:0];
    //    }
    
    // gyc change
    if (!str) {
        return nil;
    }
    NSDate *now =[NSDate date];
    NSString * newStr=[SCShareFunc dateStringToYearMonthDayFromDate:now];
    newStr =[newStr substringToIndex:5];
    newStr=[NSString stringWithFormat:@"%@%@",newStr,str];
    NSDate * now2 =[SCShareFunc dateFromTimeYearMonthDay:newStr];
    NSDate *timeNow;
    NSString *nowString =nil;
    for (int i=0; i<date; i++) {
        timeNow =[now2 dateByAddingTimeInterval:-(24*60*60)*i];
        nowString =[SCShareFunc dateStringToYearMonthDayFromDate:timeNow];
        nowString =[[nowString componentsSeparatedByString:@"年"] lastObject];
        [timeAry addObject:nowString];
    }
    return timeAry;
    
}

// 或取当前的前一天 用 04月04日 这样表示
-(NSString *)oneDayBeforeDay:(NSString *)day{
    NSDate *now =[NSDate date];
    NSString * newStr=[SCShareFunc dateStringToYearMonthDayFromDate:now];
    newStr =[newStr substringToIndex:5];
    newStr=[NSString stringWithFormat:@"%@%@",newStr,day];
    NSDate * now2 =[SCShareFunc dateFromTimeYearMonthDay:newStr];
    NSDate *timeNow;
    NSString *nowString =nil;
    
    timeNow =[now2 dateByAddingTimeInterval:-(24*60*60)*1];
    nowString =[SCShareFunc dateStringToYearMonthDayFromDate:timeNow];
    nowString =[[nowString componentsSeparatedByString:@"年"] lastObject];
    
    return nowString;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 前后页面
- (IBAction)preOrNextBtnClick:(id)sender {
  
    CGFloat pageWidth = pageScrollView.frame.size.width;
    int page = floor((pageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
        page =page+((self.prePageBtn ==(UIButton *)sender)?-1:(+1));
    if (page<0||page>3) {
        self.prePageBtn.hidden =self.nextPageBtn.hidden =YES;
        return;
    }
    
   [self.pageScrollView setContentOffset:(CGPoint){self.pageScrollView.bounds.size.width*page,0}];
    CGRect frame =self.contentView.frame;
    frame.origin.x =self.pageScrollView.bounds.size.width*page;
    
    
    [self lazyChangeScro:pageScrollView];
    
    
}

/*!
 *  根据scroll page 设置 hidden
 *
 *  @param page  from 0 to n-1
 */
-(void)setPreOrNextBtnHiddenWithPageScrollPage:(int)page{
   
    if (isLife||isSleepQua) {
         __weak UIImage * tem =[self->oneWeekButton  currentBackgroundImage];
        if ([tem  isEqual:[UIImage imageNamed:@"xuanzhong-zhou"]]) {
            // xuan zhong 一周
            self.prePageBtn.hidden =self.nextPageBtn.hidden =YES;
            return;
        }
        tem =[self->twoWeekButton currentBackgroundImage];
        if ([tem  isEqual:[UIImage imageNamed:@"xuanzhong-zhou"]]) {
            // xuan zhong 2周
            if (page==0) {
                self.nextPageBtn.hidden =NO;
                self.prePageBtn.hidden =YES;
                return;
            }else if (page ==1){
            
                self.nextPageBtn.hidden =YES;
                self.prePageBtn.hidden =NO;
                return;
            }else{
                self.prePageBtn.hidden =self.nextPageBtn.hidden =YES;
                return;
            }
        }
        tem =[self->oneMonth currentBackgroundImage];
        if ([tem  isEqual:[UIImage imageNamed:@"xuanzhong-zhou"]]) {
            // xuan zhong 2周
            if (page==0) {
                self.nextPageBtn.hidden =NO;
                self.prePageBtn.hidden =YES;
                return;
            }else if (page ==1){
                
                self.nextPageBtn.hidden =NO;
                self.prePageBtn.hidden =NO;
                return;
            }else if (page ==2){
                self.nextPageBtn.hidden =NO;
                self.prePageBtn.hidden =NO;
                return;
            }else if(page ==3){
                self.nextPageBtn.hidden =YES;
                self.prePageBtn.hidden =NO;
                return;
            }else{
            
                self.prePageBtn.hidden =self.nextPageBtn.hidden =YES;
                return;
            }
        }
        
        // 都没有
        self.nextPageBtn.hidden =self.prePageBtn.hidden =YES;
        return;
        
        
    }else if (isSleep){
        self.prePageBtn.hidden  =YES;
        self.nextPageBtn.hidden =YES;
        
    }else{
        // 都没有
        self.nextPageBtn.hidden =self.prePageBtn.hidden =YES;
        return;
    }
    //
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

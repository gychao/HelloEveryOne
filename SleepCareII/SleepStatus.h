//
//  SleepStatus.h
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCSleepEStatusView.h"
@class SCLinePrintView;

@protocol zuoyoufanObey <NSObject>

-(void)zuoyoufan:(UIButton *)btn;
@end

// scrollview 上 的大view
@interface SleepStatus : UIView

@property(nonatomic,strong)NSString *identify;


#pragma mark -状态信息
@property (weak, nonatomic) IBOutlet UIView *zhuangTaiView;

@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet SCSleepEStatusView *userInfoView;
@property (weak, nonatomic) IBOutlet UILabel *goSleepTime;

@property (weak, nonatomic) IBOutlet UILabel *wakeUpTime;

@property (weak, nonatomic) IBOutlet UILabel *outBedNum;

@property (weak, nonatomic) IBOutlet UILabel *apneaTime;


@property (weak, nonatomic) IBOutlet UILabel *dreamTime;

@property (weak, nonatomic) IBOutlet UILabel *lowSleepTime;


@property (weak, nonatomic) IBOutlet UILabel *deepSleepTime;

@property (weak, nonatomic) IBOutlet UILabel *sleepTime;

@property (weak, nonatomic) IBOutlet UILabel *dreamPercent;


@property (weak, nonatomic) IBOutlet UILabel *lowSleepPercent;

@property (weak, nonatomic) IBOutlet UILabel *deepSleepPercent;


@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;


@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;

@property (weak, nonatomic) IBOutlet SCLinePrintView *sleepRangeView;


#pragma mark -心跳
@property (weak, nonatomic) IBOutlet UIView *heartBeatView;

@property (weak, nonatomic) IBOutlet UILabel *AvarHeartLabel;

@property (weak, nonatomic) IBOutlet UIImageView *lowHeartZhenImageView;
@property (weak, nonatomic) IBOutlet UIImageView *hightHeartImageView;

@property (weak, nonatomic) IBOutlet UILabel *lowHeartLeabel;
@property (weak, nonatomic) IBOutlet UILabel *hightHeartLabel;


#pragma mark - 呼吸信息
@property (weak, nonatomic) IBOutlet UIView *breathView;

@property (weak, nonatomic) IBOutlet UIImageView *breathZhiZhenView;

@property (weak, nonatomic) IBOutlet UILabel *breathLabel;

@property (weak, nonatomic) IBOutlet UILabel *breathStop;

@property (weak, nonatomic) IBOutlet UILabel *breathLowAir;

@property (weak, nonatomic) IBOutlet UILabel *breathNoBreath;

@property (weak, nonatomic) IBOutlet UILabel *breathDaHan;




#pragma mark - 睡眠信息
@property (weak, nonatomic) IBOutlet UIView *sleepInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *shuShuiZhiZhenView;
@property (weak, nonatomic) IBOutlet UIImageView *qianShuiZhiZhenView;

@property (weak, nonatomic) IBOutlet UILabel *shuShuiLabel;
@property (weak, nonatomic) IBOutlet UILabel *qianShuiLabel;



#pragma mark - for iPhone
@property (weak, nonatomic) IBOutlet UILabel *sleepRangeLabel;
@property (weak, nonatomic) IBOutlet UIButton *zuofan;

@property (weak, nonatomic) IBOutlet UIButton *youfan;

@property(nonatomic,assign)id<zuoyoufanObey>vcDelegate;


// pa 做其他补充， 传nil
-(void)displayItsViewUseData:(NSDictionary *)dataDic andWithIdentify:(NSString *)identify andOtherParam:(id)pa;


-(void)displayViewUse:(NSDictionary *)reponse withNumTag:(int)numIndex;

@end

//
//  SCSleepEStatusView.m
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCSleepEStatusView.h"

//各数值含义：7 非睡眠， 6、5 醒/做梦， 4、3 浅睡眠，2、1 深睡眠
//endTime	:	23:13:00   sleepStatus	:	5  startTime	:	23:13:00


#define color_lc [UIColor colorWithRed:255 green:255 blue:255 alpha:1] //离床
#define color_mx [UIColor colorWithRed:8 green:48 blue:253 alpha:1]  //梦醒
#define color_qs [UIColor colorWithRed:255 green:193 blue:5 alpha:1]  //浅睡
#define color_ss [UIColor colorWithRed:7 green:251 blue:6 alpha:1]  //深睡
@interface SCSleepEStatusView ()

@property(nonatomic,strong)NSArray *inBedStatusList;
@property(nonatomic,strong)NSArray *inBedTimeList;
@end


@implementation SCSleepEStatusView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    
    }
    return self;
}

-(void)drawSelfUseDataDic:(NSDictionary *)dic withAnotherUsePara:(id)pa{
    NSLog(@"%@,%s",dic,__func__);
    self.inBedTimeList =nil;
    self.inBedStatusList =nil;
    self.inBedStatusList =[dic objectForKey:@"inBedStatusList"];
    self.inBedTimeList   =[dic objectForKey:@"inBedTimeList"];
    
    // 此方法 是实际设备中调试的，没有什么原因。 自己调试准效果
    if (isIPhonePlat) {
        if (isIPhone5) {
            CGRect frame =self.frame;
            frame.origin.x-=1;
            frame.size.width+=3;
//            frame.origin.y+=0.7;
//            frame.size.height -=0.7;
            self.frame =frame;
        }else{
            //4
//            CGRect frame =self.frame;
//            frame.origin.x+=0.9;
//            frame.size.width-=0.9;
//            
//            self.frame =frame;
        }
    }else{
        CGRect frame =self.frame;
        frame.origin.x-=1.8;
        frame.size.width+=2;
        self.frame =frame;
    }
    
    [self setNeedsDisplay];
    
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
#define K24Hour2Sec (24*60*60)
- (void)drawRect:(CGRect)rect
{
//    self.inBedTimeList =@[@"00:00:00",
//                          @"12:00:00"];
    // Drawing code
//    self.backgroundColor =[UIColor blueColor];// for test
    
    NSLog(@"rect %@",NSStringFromCGRect(rect));
    CGContextClearRect(UIGraphicsGetCurrentContext(), rect);
    [self setClearsContextBeforeDrawing: YES];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    NSDate *date =[SCShareFunc hour_dateFromString:@"12:00:00"];
    NSDate *date_night =[SCShareFunc hour_dateFromString:@"00:00:00"];
    NSDate *startDate =nil,*endDate;
    NSString *startTime,*endTime;
    int sleepStatus;
    double interval;
    double startPoint_x,endPoint_x, inBed_startPoint_x,inBed_endPoint_x;
    

    
    //画睡眠时间段 蓝色
    if (self.inBedTimeList&&self.inBedTimeList.count>=2) {
        double Kmin =((120.0)/K24Hour2Sec )*self.bounds.size.width;
        
        for (int j=0; j<self.inBedTimeList.count-1; j+=2) {
            
            NSString *goSleep =self.inBedTimeList[j];
            NSString *wakeUp =self.inBedTimeList[j+1];
            startDate =[SCShareFunc hour_dateFromString:goSleep];
            endDate  =[SCShareFunc hour_dateFromString:wakeUp];
            
            // 首先得到 这个时间段 在view上位置
            if ([goSleep compare:@"12:00:00" options:NSNumericSearch]==NSOrderedAscending||NSOrderedSame) {
                interval =[startDate timeIntervalSinceDate:date_night] +12*60*60;
                
            }else{
                
                interval =[startDate  timeIntervalSinceDate:date];
            };
            // 起始位置
           
            inBed_startPoint_x = (fabs(interval)/K24Hour2Sec )*self.bounds.size.width;
            
            if ([wakeUp compare:@"12:00:00" options:NSNumericSearch]==NSOrderedAscending||NSOrderedSame) {
                interval =[endDate timeIntervalSinceDate:date_night] +12*60*60;
                
            }else{
                interval =[endDate  timeIntervalSinceDate:date];
                if ([wakeUp isEqualToString:@"12:00:00"]) {
                     interval =12*60*60 +12*60*60;
                }
            };
            // 中止位置
            inBed_endPoint_x =(fabs(interval)/K24Hour2Sec )*self.bounds.size.width;
          
             NSLog(@"一段睡眠时间起始点: %f,%f,bounds:%@,rect: %@",inBed_startPoint_x,inBed_endPoint_x, NSStringFromCGRect(self.bounds), NSStringFromCGRect(rect));
            
            if (inBed_endPoint_x-inBed_startPoint_x<=Kmin) {
                // 小于等于 2分钟 不画
                if (self.inBedStatusList) {
                    continue;
                }else{
                    CGContextSetRGBFillColor(context,0.1686, 0.0000, 0.9647,1);
                    CGContextFillRect(context, CGRectMake(inBed_startPoint_x, 0, inBed_endPoint_x-inBed_startPoint_x, self.bounds.size.height));
                }
               
                
            }else{
                
                 CGContextSetRGBFillColor(context,0.1686, 0.0000, 0.9647,1);
                 CGContextFillRect(context, CGRectMake(inBed_startPoint_x, 0, inBed_endPoint_x-inBed_startPoint_x, self.bounds.size.height));
                
                
            }
        }
        
                /*!
                 *  core draw
                 */
                if (self.inBedStatusList) {
                    for (NSDictionary  *UserSleepActivityInfo in self.inBedStatusList) {
                        // \
                        startTime  sleepStatus  endTime
                        startTime =[UserSleepActivityInfo objectForKey:@"startTime"];
                        endTime   =[UserSleepActivityInfo objectForKey:@"endTime"];
                        sleepStatus     =[[UserSleepActivityInfo objectForKey:@"sleepStatus"] intValue];
                        
                        startDate =[SCShareFunc hour_dateFromString:startTime];
                        endDate  =[SCShareFunc hour_dateFromString:endTime];
                        
                        if ([startTime compare:@"12:00:00" options:NSNumericSearch]==NSOrderedAscending||NSOrderedSame) {
                            interval =[startDate timeIntervalSinceDate:date_night] +12*60*60;
                            
                        }else{
                            
                            interval =[startDate  timeIntervalSinceDate:date];
                        };
                        // 起始位置
                        NSLog(@"%f,%@",interval/K24Hour2Sec,NSStringFromCGRect(self.bounds));
                        startPoint_x = (fabs(interval)/K24Hour2Sec )*self.bounds.size.width;
                        
                        if ([endTime compare:@"12:00:00" options:NSNumericSearch]==NSOrderedAscending||NSOrderedSame) {
                            interval =[endDate timeIntervalSinceDate:date_night] +12*60*60;
                            
                        }else{
                            interval =[endDate  timeIntervalSinceDate:date];
                            if ([endTime isEqualToString:@"12:00:00"]) {
                                interval =12*60*60*2;
                            }
                        };
                        // 中止位置
                        endPoint_x =(fabs(interval)/K24Hour2Sec )*self.bounds.size.width;
                        
                        if (startPoint_x==endPoint_x) {
                            continue;
                        }
                        
                        
//                        在这个Action中，在床状态的状态码sleepStatus的意思是：1:dream-56, 2:low-4, 3:deep-3, 4:deep-12, 5: not sleep-7
                        // 自定义绘制颜色
                        switch (sleepStatus) {
                            case 1:{//蓝色
                                CGContextSetRGBFillColor(context,0.1686, 0.0000, 0.9647,1);
                            }break;
                            case 2:
                            {   // 橙色
                                CGContextSetRGBFillColor(context,0.9333, 0.7725, 0.2902,1);
                            }break;
                            case 3:{// 浅绿色
                                CGContextSetRGBFillColor(context, 0.2039, 1.0000, 0.1412, 1);
                            }break;
                            case 4:{ // 深绿色
                                CGContextSetRGBFillColor(context,0.3098, 0.6510, 0.0824,1);
                            }
                            case 5:{
                                    //  白色 logic 5不用画
//                                CGContextSetRGBFillColor(context, 1, 1, 1, 1);
                            }
                                break;
                            default:
                                break;
                        }
                        if (sleepStatus!=5) {
                             CGContextFillRect(context, CGRectMake(startPoint_x, 0, endPoint_x-startPoint_x, self.bounds.size.height));
                        }
                       
                        
                        
                    }
                    CGContextStrokePath(context);
                    
                }
            }
            
            
        }
//        
//    }

    //
    
    
//    }


@end

//
//  SCCurrentStatusVC.m
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCCurrentStatusVC.h"
#import "SCCurrentStatusView.h"

@interface SCCurrentStatusVC ()

@end

@implementation SCCurrentStatusVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#define  kViewNum 2
-(UIScrollView *)scrollView{
    if (!_scrollView) {
        UIScrollView *scrollView =[[UIScrollView alloc] initWithFrame:(CGRect){0,self.currentBtn.bounds.size.height,self.view.bounds.size.width,self.view.bounds.size.height -self.currentBtn.bounds.size.height}];
        [self.view insertSubview:scrollView atIndex:0];
        self.scrollView =scrollView;
        
        NSString *viewName =nil;
        if ([SCShareFunc isIPhone]) {
            viewName =@"SCCurrentStatusView_iPhone";
        }else{
            viewName =@"SCCurrentStatusView_iPad";
        }
        
        SCCurrentStatusView *statusView =nil;
        NSArray *viewArr =[[NSBundle mainBundle] loadNibNamed:viewName owner:nil options:nil];
        for (id view in viewArr) {
            if ([view isKindOfClass:[SCCurrentStatusView class]]) {
                statusView  =view;
                break;
            }
        }
        statusView.frame =scrollView.bounds;
        [scrollView addSubview:statusView];
        statusView.tag =(scrollView.contentOffset.y/scrollView.bounds.size.width)+1;
       
        
        statusView.layer.borderWidth =0.5;
        statusView.layer.borderColor =[[UIColor colorWithRed:46.0/255 green:138.0/255 blue:208.0/255 alpha:1.0]CGColor];
        
        scrollView.delegate =self;
        [scrollView setContentSize:(CGSize){kViewNum*scrollView.bounds.size.width,scrollView.bounds.size.height}];
        [scrollView setContentOffset:(CGPoint){0,0}];
        
        
        
        scrollView.pagingEnabled =YES;
        scrollView.showsVerticalScrollIndicator =NO;
        scrollView.bounces =NO;
        
    }
    
    return _scrollView;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.visibleViewArr =[NSMutableArray array];
    self.currentBtn.selected =YES;
    self.Last20MinBtn.selected =NO;
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    int numIndex = (int)(self.scrollView.contentOffset.x/self.scrollView.bounds.size.width) +1;
    SCCurrentStatusView *csv =(SCCurrentStatusView *)[self.scrollView viewWithTag:numIndex];
    [csv displayViewUse:nil withNumTag:numIndex];
    
}


- (IBAction)clickBtnToChangeStatus:(UIButton *)sender {
    
    if (sender.selected) {
        return;
    }
    self.currentBtn.selected =self.Last20MinBtn.selected =NO;
    sender.selected =YES;
    if (sender ==self.currentBtn) {
       [ self.scrollView setContentOffset:(CGPoint){0,0}];
      
    }else if (sender ==self.Last20MinBtn){
      [ self.scrollView setContentOffset:(CGPoint){self.scrollView.bounds.size.width,0}];
    }
    
    [self LazyChangeScrollView:self.scrollView OfItsView:nil];
}


#pragma mark -scrollViewDelegate
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        [self LazyChangeScrollView:scrollView OfItsView:nil];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self LazyChangeScrollView:scrollView OfItsView:nil];
}

// para can nil to be use future
-(void)LazyChangeScrollView:(UIScrollView *)scrollView OfItsView:(id)Para{

    //
    NSInteger numIndex =
            (NSInteger) scrollView.contentOffset.x/scrollView.bounds.size.width+1;
    
    self.currentBtn.selected =self.Last20MinBtn.selected =NO;
    if (numIndex==1) {
        self.currentBtn.selected =YES;
    }else if (numIndex ==2){
        self.Last20MinBtn.selected =YES;
    }else{
        self.currentBtn.selected =YES;
    }
    
    
    SCCurrentStatusView *csv =(SCCurrentStatusView *)[scrollView viewWithTag:numIndex];
    if (!csv) {
        
        [self.visibleViewArr removeAllObjects];
        for (id temView in self.scrollView.subviews) {
            if ([temView isKindOfClass:[SCCurrentStatusView class]]) {
                [self.visibleViewArr insertObject:temView atIndex:0];
                [temView removeFromSuperview];
            }
            
        }
        
        if (self.visibleViewArr.count) {
            csv =self.visibleViewArr[0];
        }else{
            NSString *viewName =nil;
            if ([SCShareFunc isIPhone]) {
                viewName =@"SCCurrentStatusView_iPhone";
            }else{
                viewName =@"SCCurrentStatusView_iPad";
            }
            csv=[[[NSBundle mainBundle] loadNibNamed:viewName owner:nil options:nil] lastObject];
        }
    }else{
    
        // 存在
        return;
    }
    csv.tag =numIndex;
    CGRect frame;
    frame.origin =(CGPoint){scrollView.contentOffset.x,0};
    frame.size =scrollView.bounds.size;
    csv.frame =frame;
    [scrollView addSubview:csv];
    
    [csv displayViewUse:nil withNumTag:numIndex];
        
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

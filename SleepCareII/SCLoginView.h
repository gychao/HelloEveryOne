//
//  SCLoginView.h
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>
enum LoginViewDisplayStyle{
    loginStyle        =1,
    findPasswordStyle =1<<1,
    getPasswordStyle  =1<<2
};
typedef int LoginViewDisplayStyle;

@interface SCLoginView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *autoLoginBtn;

@property (weak, nonatomic) IBOutlet UIButton *forgetPasswordBtn;


@property (weak, nonatomic) IBOutlet UIButton *loginBtn;


@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;


@property (weak, nonatomic) IBOutlet UIButton *getPasswordBtn;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UILabel *getPWLabel;



@property (weak, nonatomic) IBOutlet UIButton *knowPWBtn;

-(void)displaySelfWithType:(LoginViewDisplayStyle)style;



@end

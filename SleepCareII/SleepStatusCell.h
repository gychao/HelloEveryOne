//
//  SleepStatusCell.h
//  SleepCareII
//
//  Created by mengqinghao on 14-6-24.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SCSleepEStatusView;

@interface SleepStatusCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UILabel * dateTimeLabel;

@property (weak, nonatomic) IBOutlet SCSleepEStatusView *drawSleepInBed;
@property (weak, nonatomic) IBOutlet UIView *spertorView;

-(void)displayTime:(NSArray*)Timeary dateTime:(NSString*)date;

@end

//
//  SCCurrentStatusVC.h
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"

@interface SCCurrentStatusVC : SCBaseViewController
@property (weak, nonatomic) IBOutlet UIButton *currentBtn;

@property (weak, nonatomic) IBOutlet UIButton *Last20MinBtn;



@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)NSMutableArray *visibleViewArr;
@end

//
//  SCTiXingVC.h
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"
#import "NoticeCell.h"
@interface SCTiXingVC : SCBaseViewController<UITableViewDataSource,UITableViewDelegate,deleteTixingDelegate>
{
    NSMutableArray * dataAry;// 提醒列表 ary
}

@property(nonatomic,strong)NSMutableArray * deleteAry;// 选中需要删除的数组 mqh 2014-6-20

@property(nonatomic,strong) IBOutlet UITableView * noticeTab;//
@property(nonatomic,strong) IBOutlet UIButton * deleteButton;// 删除按钮

-(IBAction)deleteButtonClick:(id)sender;


#pragma mark - for iphone5
@property (weak, nonatomic) IBOutlet UIView *dataView;


@end

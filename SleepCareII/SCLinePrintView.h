//
//  SCLinePrintView.h
//  SleepCareII
//
//  Created by dilitech on 14-6-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCDrawView.h"

@interface SCLinePrintView : SCDrawView{

    int x;
    int y;
}

//点信息
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) NSArray* array1;
@property (strong, nonatomic) IBOutlet UILabel *sleepRangeLabel;

// pa user for future
-(void)drawSelfUseDataDic:(NSDictionary *)dic withAnotherUsePara:(id)pa;
@end

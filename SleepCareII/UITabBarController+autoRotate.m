//
//  UITabBarController+autoRotate.m
//  Furniture_basic
//
//  Created by ziheli on 12-11-26.
//
//

#import "UITabBarController+autoRotate.h"

@implementation UITabBarController (autoRotate)
#ifdef __IPHONE_6_0
-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}
#endif
@end

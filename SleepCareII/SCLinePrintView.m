//
//  SCLinePrintView.m
//  SleepCareII
//
//  Created by dilitech on 14-6-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCLinePrintView.h"

#define kDes_X  (isIPhonePlat?30:50)      //  x  左边距离
#define kDes_Y  (isIPhonePlat?20:30)      //  y  下边距离

#define kDes_X_right 25 // x 轴 右边距离

#define kDes_Y_up  (isIPhonePlat?15:5)// 距离titil 的

#define Add_X 5  // x 轴 突出来的一部分

#define KVerLabel_Width 40
#define c_LastYear       [UIColor colorWithRed:98.0/255.0 green:187.0/255.0 blue:71.0/255.0 alpha:1.0];
// 前年_蓝
#define c_BeforeLastYear [UIColor colorWithRed:0.0/255.0 green:157.0/255.0 blue:220.0/255.0 alpha:1.0];

#define fontSize    (isIPhonePlat?8:10)


@implementation SCLinePrintView

-(id)initWithFrame:(CGRect)frame{

    self =[super initWithFrame:frame];
    if (self) {
       
    }
    return self;
}

-(void)layoutSubviews{
    self.backgroundColor =[UIColor whiteColor];
    CGRect frame =self.frame;
    x = frame.size.width -kDes_X-kDes_X_right;
    y = frame.size.height-kDes_Y;
    
    if (!_titleLabel) {
        _titleLabel =[[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.textAlignment =TextAliCenter;
        _titleLabel.textColor =[UIColor blackColor];
        _titleLabel.backgroundColor =[UIColor clearColor];
        
    }
    if (!_titleLabel.superview) {
        [self addSubview:_titleLabel];
    }
}

-(void)drawSelfUseDataDic:(NSDictionary *)dic withAnotherUsePara:(id)pa{
    self.array =nil;
      self.array =[dic objectForKey:@"sleepRanges"];
     [self setNeedsDisplay];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#define K24Hour2Sec (24*60*60)
- (void)drawRect:(CGRect)rect
{
    
    [super drawRect:rect];
  
    if (!_array) {
        return;
    }
    [self.subviews  makeObjectsPerformSelector:@selector(removeFromSuperview)];

    [self addSubview:self.sleepRangeLabel];
    
   
    
    [self setClearsContextBeforeDrawing: YES];
    
    CGContextRef context = UIGraphicsGetCurrentContext();

    
//    //画背景线条------------------
    CGColorRef backColorRef = [UIColor redColor].CGColor;
    CGFloat backLineWidth = 0.5f;
    CGFloat backMiterLimit = 0.f;
    
    CGContextSetLineWidth(context, backLineWidth);//主线宽度
    
    CGContextSetLineJoin(context, kCGLineJoinRound);
    
    CGContextSetLineCap(context, kCGLineCapRound );
    
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    
    // 先画一个白色方框
    //
    

    int tempY = y;
    

    //添加纵轴标签和线
    for (int i=0; i<self.vDesc.count; i++) {
        
        CGPoint bPoint = CGPointMake(kDes_X, tempY);
        CGPoint ePoint = CGPointMake(x, tempY);
        tempY -= (y-_titleLabel.bounds.size.height-kDes_Y_up)/(self.vDesc.count);
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, kDes_X, kDes_X)];
        [label setCenter:CGPointMake(bPoint.x-kDes_X/2,tempY)];
        
        NSLog(@"center %@",NSStringFromCGPoint(label.center));
        
        label.textAlignment =TextAliRight;
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor blackColor]];
        [label setText:[self.vDesc  objectAtIndex:i]];
        label.font =[UIFont systemFontOfSize:fontSize];
        [self addSubview:label];
        // 竖直轴 突出部分
        CGContextMoveToPoint(context, kDes_X, y-(y-_titleLabel.bounds.size.height-kDes_Y_up)/(self.vDesc.count)*i);
        CGContextAddLineToPoint(context, kDes_X-Add_X/2, y-(y-_titleLabel.bounds.size.height-kDes_Y_up)/(self.vDesc.count)*i);
        
        if (i==0) {
            NSLog(@"self.bounds %@, start %@ ,end %@",NSStringFromCGRect(self.frame),NSStringFromCGPoint((CGPoint){bPoint.x,y}),NSStringFromCGPoint((CGPoint){x+kDes_X,y}));
            CGContextMoveToPoint(context, bPoint.x, y);
            CGContextAddLineToPoint(context, x+kDes_X, y); //横轴
            
            CGContextMoveToPoint(context, bPoint.x,y);
            CGContextAddLineToPoint(context, bPoint.x, _titleLabel.bounds.size.height+kDes_Y_up); //竖轴
        }
       
        
    }
    
    // 补充画水平轴
    float KlineHeight =(CGFloat)x/(self.hDesc.count-1);
    float Add_Y =5;
    for (int i=0; i<self.hDesc.count; i++) {
        
        UILabel *label = [[UILabel  alloc] init ];//WithFrame:CGRectMake(i*KlineHeight+30, y+Add_Y,0 ,0)];
        [label setTextAlignment:TextAliCenter];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextColor:[UIColor blackColor]];
        label.numberOfLines = 1;
        label.font =[UIFont systemFontOfSize:fontSize];
        [label setText:[self.hDesc objectAtIndex:i]];
        [label sizeToFit];
        
        //得到此时的label 宽度
        CGFloat width =label.bounds.size.width;
        NSAssert(width>0, @"label width must > 0");
        CGRect rect =(CGRect){(i*KlineHeight+kDes_X)-width/2,y+Add_Y,label.bounds.size.width,label.bounds.size.height};
        label.frame =rect;
      
        // 画突冒出来的一部分
        CGContextMoveToPoint(context, i*KlineHeight+kDes_X, y);
        CGContextAddLineToPoint(context, i*KlineHeight+kDes_X, y+Add_Y/2);
        if (i!=self.hDesc.count-1) {
            CGContextMoveToPoint(context, i*KlineHeight+kDes_X+KlineHeight/2, y);
            CGContextMoveToPoint(context, i*KlineHeight+kDes_X+KlineHeight/2, y+Add_Y/2);
        }
        
        [self addSubview:label];
    }
    CGContextStrokePath(context);
    
    
    
    //    //画点线条------------------
//    CGColorRef pointColorRef = [UIColor colorWithRed:24.0f/255.0f green:116.0f/255.0f blue:205.0f/255.0f alpha:1.0].CGColor;
    CGFloat pointLineWidth = 1.5f;
    CGFloat pointMiterLimit = 5.0f;
    
    CGContextSetLineWidth(context, pointLineWidth);//主线宽度
    CGContextSetMiterLimit(context, pointMiterLimit);//投影角度
    
    
    CGContextSetLineJoin(context, kCGLineJoinRound);
    
    CGContextSetLineCap(context, kCGLineCapRound );
    
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    
    //gyc 绘图
    NSDate *date =[SCShareFunc hour_dateFromString:@"12:00:00"];
    NSDate *date_night =[SCShareFunc hour_dateFromString:@"00:00:00"];
    NSDate *startDate =nil,*endDate;
    NSString *startTime,*endTime;
    int sleepStatus;
    double interval;
    double startPoint_x,endPoint_x;
    double endPoint_Y;
    NSLog(@"%@",_array);
    
    CGContextSetShouldAntialias(context,YES);       // 抗锯齿
    
    for (int j=0;j<_array.count;j++) {
        //startTime  sleepStatus  endTime
        NSDictionary *UserSleepActivityInfo =_array[j];
        startTime =[UserSleepActivityInfo objectForKey:@"startTime"];
        endTime   =[UserSleepActivityInfo objectForKey:@"endTime"];
        sleepStatus     =[[UserSleepActivityInfo objectForKey:@"sleepStatus"] intValue];
        
        startDate =[SCShareFunc hour_dateFromString:startTime];
        endDate  =[SCShareFunc hour_dateFromString:endTime];
        
        if ([startTime compare:@"12:00:00" options:NSNumericSearch]==NSOrderedAscending||NSOrderedSame) {
            interval =[startDate timeIntervalSinceDate:date_night] +12*60*60;
            
        }else{
            interval =[startDate  timeIntervalSinceDate:date];
        };
        // 起始位置
        NSLog(@"%f,%@",interval/K24Hour2Sec,NSStringFromCGRect(self.bounds));
        startPoint_x = (interval/K24Hour2Sec )*x+kDes_X;
        
        if ([endTime compare:@"12:00:00" options:NSNumericSearch]==NSOrderedAscending||NSOrderedSame) {
            interval =[endDate timeIntervalSinceDate:date_night] +12*60*60;
            
        }else{
            interval =[endDate  timeIntervalSinceDate:date];
        };
        // 中止位置
        endPoint_x =(interval/K24Hour2Sec )*x+kDes_X;
        
        // 自定义绘制颜色
        switch (sleepStatus) {
            default:{
                endPoint_Y = y-  (y-_titleLabel.bounds.size.height-kDes_Y_up)/self.vDesc.count *sleepStatus;
            }
                break;
        }

       
        
        if (j!=0) {
           [[UIColor lightGrayColor] setStroke];
            CGContextAddLineToPoint(context, startPoint_x, endPoint_Y);
             CGContextStrokePath(context);
        }

        CGContextMoveToPoint(context, startPoint_x, endPoint_Y);
        
        if (startPoint_x==endPoint_x) {
            // 起始初始位置相等 画一个点
           
        }else{
            CGContextSetRGBStrokeColor(context, 1, 0, 0, 1);
            CGContextAddLineToPoint(context, endPoint_x, endPoint_Y);
             CGContextStrokePath(context);
            CGContextMoveToPoint(context, endPoint_x, endPoint_Y);
            
        }
        
        

    }
     
  
}

@end

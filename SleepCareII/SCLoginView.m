//
//  SCLoginView.m
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCLoginView.h"

@implementation SCLoginView

-(void)awakeFromNib{
    NSLog(@"%s",__func__);
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)displaySelfWithType:(LoginViewDisplayStyle)style{
    [self.subviews makeObjectsPerformSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES]];
     self.bgImageView.hidden =NO;
    switch (style) {
        case loginStyle:
        {
            self.nameLabel.hidden =self.nameTextField.hidden =NO;
            self.passwordLabel.hidden =self.passwordTextField.hidden =NO;
            self.autoLoginBtn.hidden =self.forgetPasswordBtn.hidden =NO;
            self.loginBtn.hidden  =self.cancelBtn.hidden =NO;
        
            self.passwordLabel.text =@"密  码 ：";
            self.passwordTextField.secureTextEntry =YES;
            
        }
            break;
        case findPasswordStyle:{
            self.nameLabel.hidden =self.nameTextField.hidden =NO;
            self.passwordLabel.hidden =self.passwordTextField.hidden =NO;
            self.getPasswordBtn.hidden =self.backBtn.hidden =NO;
            
            self.passwordLabel.text =@"使用者姓名 ：";
            self.passwordTextField.secureTextEntry =NO;
            self.passwordTextField.text =nil;
            
        }break;
            
        case getPasswordStyle:{
            self.getPWLabel.hidden =NO;
            self.knowPWBtn.hidden =NO;
        }
            break;
        default:{
            self.nameLabel.hidden =self.nameTextField.hidden =NO;
            self.passwordLabel.hidden =self.passwordTextField.hidden =NO;
            self.autoLoginBtn.hidden =self.forgetPasswordBtn.hidden =NO;
            self.loginBtn.hidden  =self.cancelBtn.hidden =NO;
            self.passwordLabel.text =@"密  码 ：";
        }
            break;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

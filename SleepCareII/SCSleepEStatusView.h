//
//  SCSleepEStatusView.h
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>


// just Draw sleepE
// 只是画线条
@interface SCSleepEStatusView : UIView

// pa can nil be for feature
-(void)drawSelfUseDataDic:(NSDictionary *)dic withAnotherUsePara:(id)pa;

@end

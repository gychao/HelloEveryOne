//
//  SCShareFunc.h
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCShareFunc : NSObject


+(BOOL)isNotEmptyStringOfObj:(id)obj;////是非空字符串

+(BOOL)systemVersonIsAfterNumber:(float)number; //判断版本号 是几之后

+(BOOL)isIPhone;


+(void)displayHub;
+(void)dismissHub;

+(NSString *)getServerUrlString;

+(NSString *)stringFromDate:(NSDate *)date;
+(NSDate *)dateFromString:(NSString *)dateString;
+(NSDate *)hour_dateFromString:(NSString *)dateString;
+(NSString *)hour_stringFromDate:(NSDate *)date;
+(NSDate *)dateFromTimeYearMonthDay:(NSString *)dateString;
+(NSString *)dateStringToYearMonthDayFromDate:(NSDate *)date;

+(NSString *)firstDayStrFromMonth:(NSDate *)moth;


+(int)dayNumFormDateStr:(NSString *)str;//str : 2014-06-05

+(NSString *)nameWithInstance:(id)inten  andInstanceOwner:(id)target;//根据 对象引用 得到对象变量名称

+(NSString *)dateStringToMonthDayFromDate:(NSDate *)date;


@end

//
//  SCLoginLogicManager.h
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCLoginLogicManager : NSObject{

}
@property(nonatomic,strong)NSString *title;
+(void)startAppAtDelegate:(id)delegate;

+(void)changeWindowRootVCToAfterLogin:(BOOL)afterlogin  orToLoginVC:(BOOL)login;
@end

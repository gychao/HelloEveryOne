//
//  NoticeCell.m
//  SleepCareII
//
//  Created by mengqinghao on 14-6-19.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "NoticeCell.h"

#import "UIButton+openTapArea.h"

@implementation NoticeCell
@synthesize dataAry;
@synthesize delegate;
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)displayDic:(NSDictionary *)mdic indexNum:(NSInteger)num
{
    selectNum=num;
    self.dateLabel.text=[mdic objectForKey:@"date"];
    self.titleLabel.text=[mdic objectForKey:@"title"];
    self.contentLabel.text=[mdic objectForKey:@"content"];
    NSNumber *isselect =[mdic objectForKey:@"isSelect"];
    if (isselect&&[isselect isKindOfClass:[NSNumber class]]) {
        if ([isselect intValue]) {
            self.deletebutton.selected =YES;
        }else{
            self.deletebutton.selected =NO;
        }
    }else{
        self.deletebutton.selected =NO;
    }
}
-(IBAction)deleteButtonClick:(UIButton*)btn
{
    btn.selected=!btn.selected;
    if (btn.selected) {
//        [btn setBackgroundImage:[UIImage imageNamed:@"xuanzhong"] forState:UIControlStateSelected]; ／／在按钮中已经设置
        NSMutableDictionary * dic =[dataAry objectAtIndex:selectNum];
        [dic setObject:[NSNumber numberWithInt:1] forKey:@"isSelect"];
//        [self.delegate selectByAry:dataAry];
        
    }else {
//        [btn setBackgroundImage:[UIImage imageNamed:@"weixuanzhong"] forState:0];
        NSMutableDictionary * dic =[dataAry objectAtIndex:selectNum];
        [dic setObject:[NSNumber numberWithInt:0] forKey:@"isSelect"];
//           [self.delegate selectByAry:dataAry];
    }

}

@end

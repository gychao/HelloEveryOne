//
//  UILabel+DrawTextAlignment.m
//  SleepCareII
//
//  Created by dilitech on 14-7-4.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "UILabel+DrawTextAlignment.h"
#import <objc/runtime.h>


char* const ASSOCIATION_MUTABLE_USER_INFO = "ASSOCIATION_MUTABLE_USER_INFO";
@implementation UILabel (DrawTextAlignment)
- (void)setVerticalAlignment:(VerticalAlignment)verticalAlignment {
    
    objc_setAssociatedObject(self, ASSOCIATION_MUTABLE_USER_INFO, [NSNumber numberWithInt:(int)verticalAlignment], OBJC_ASSOCIATION_ASSIGN);
    
    
//    [self drawTextInRect:textRect];
   
//    [self setNeedsDisplay];
}
//-(void)setText:(NSString *)text{
//    CGSize size =[text sizeWithFont:[UIFont systemFontOfSize:8] forWidth:300 lineBreakMode:NSLineBreakByCharWrapping];
//    int ver =[objc_getAssociatedObject(self,ASSOCIATION_MUTABLE_USER_INFO) intValue];
//    CGRect textRect =(CGRect){0,0,size.width,size.height};
//    CGRect bounds =self.bounds;
//   switch (ver) {
//      case VerticalAlignmentTop:
//                textRect.origin.y = bounds.origin.y;
//                break;
//            case VerticalAlignmentBottom:
//                textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height;
//             break;
//          case VerticalAlignmentMidele:
//            // Fall through.
//         default:
//              textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0;
//      }
//   
//    [self drawTextInRect:textRect];
//}
//- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
//    CGRect textRect = [];
//    int ver =[objc_getAssociatedObject(self,ASSOCIATION_MUTABLE_USER_INFO) intValue];
//    
//    switch (ver) {
//        case VerticalAlignmentTop:
//            textRect.origin.y = bounds.origin.y;
//            break;
//        case VerticalAlignmentBottom:
//            textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height;
//            break;
//        case VerticalAlignmentMidele:
//            // Fall through.
//        default:
//            textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0;
//    }
//    return textRect;
//}

//-(void)drawTextInRect:(CGRect)requestedRect {
////    CGRect actualRect = [self textRectForBounds:requestedRect limitedToNumberOfLines:self.numberOfLines];
//    NSString *text =self.text;
//    CGSize size =[text sizeWithFont:[UIFont systemFontOfSize:8] forWidth:300 lineBreakMode:NSLineBreakByCharWrapping];
//    int ver =[objc_getAssociatedObject(self,ASSOCIATION_MUTABLE_USER_INFO) intValue];
//    CGRect textRect =(CGRect){0,0,size.width,size.height};
//    CGRect bounds =self.bounds;
//    switch (ver) {
//        case VerticalAlignmentTop:
//            textRect.origin.y = bounds.origin.y;
//            break;
//        case VerticalAlignmentBottom:
//            textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height;
//            break;
//        case VerticalAlignmentMidele:
//            // Fall through.
//        default:
//            textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0;
//    }
//
//    [ drawTextInRect:textRect];
//}

@end

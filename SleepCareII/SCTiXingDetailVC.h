//
//  SCTiXingDetailVC.h
//  SleepCareII
//
//  Created by mengqinghao on 14-6-19.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"

@interface SCTiXingDetailVC : SCBaseViewController<UIAlertViewDelegate>
{
    NSInteger regID;

    IBOutlet UITextView * textview;
    IBOutlet UILabel * titleLabel;
}
@property(nonatomic,strong)IBOutlet UIButton * backButton;//返回按钮
@property(nonatomic,strong)IBOutlet UIButton * deleteButton;// 删除按钮
@property(nonatomic,strong)IBOutlet UITextView * textview;//
@property(nonatomic,strong)IBOutlet UILabel * titleLabel;// 标题label

@property(nonatomic,strong)NSDictionary * mdic;//

-(IBAction)backbtnClick:(id)sender;
-(IBAction)deleteButtonClick:(id)sender;
-(void)displayDic:(NSDictionary*)dic;


#pragma mark - for iphone5
@property (weak, nonatomic) IBOutlet UIView *dataView;
@end

//
//  SCTiXingDetailVC.m
//  SleepCareII
//
//  Created by mengqinghao on 14-6-19.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCTiXingDetailVC.h"
#import "SCNetManager.h"
#import "SCRootViewController.h"

@interface SCTiXingDetailVC ()

@end

@implementation SCTiXingDetailVC
@synthesize mdic;
@synthesize textview;
@synthesize titleLabel;
@synthesize backButton;
@synthesize deleteButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (isIPhonePlat) {
        if (isIPhone5) {
            CGRect bounds =  [(SCRootViewController *)[[UIApplication sharedApplication] keyWindow].rootViewController coreTableBgView].bounds;
            CGRect frame =self.dataView.frame;
            frame.size =(CGSize){bounds.size.width,bounds.size.height -20};// -20 是距离顶部 20
            self.dataView.frame =frame;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    NSLog(@"textview======%@",self.textview);
    textview.text=[self.mdic objectForKey:@"content"];
    titleLabel.text=[self.mdic objectForKey:@"title"];
    regID=[[self.mdic objectForKey:@"regId"]integerValue];
    
    self.textview.layer.borderWidth=2.0;
    self.textview.layer.borderColor=[[UIColor colorWithRed:46.0/255 green:138.0/255 blue:208.0/255 alpha:1.0]CGColor];
    self.textview.layer.cornerRadius=5.0;
}
-(IBAction)backbtnClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)displayDic:(NSDictionary*)dic
{
    self.mdic=[NSDictionary dictionaryWithDictionary:dic];
    
}
-(IBAction)deleteButtonClick:(id)sender
{
    // 方法已写 需要服务器验证。
    if (regID>0) {
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"确定删除" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert show];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        NSArray * ary =[NSArray arrayWithObject:[NSNumber numberWithInt:regID]];
        [SCNetManager deleteSleepRemindSuccess:^(BOOL success, NSDictionary *response) {
            NSLog(@"%@",response);
            NSInteger  errorcode=[[response objectForKey:@"errorCode"]integerValue];
            if (errorcode==-1100) {
//                UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"删除成功" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//                [alert show];
               [self.navigationController popViewControllerAnimated:YES];
            }else{
//                UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"参数有误,删除失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//                [alert show];
            }
            
        } faileture:^(BOOL faileTure) {
            
        } withAry:ary andCount:ary.count];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

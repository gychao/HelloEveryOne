//
//  SCDrawView.m
//  SleepCareII
//
//  Created by dilitech on 14-6-17.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCDrawView.h"
#import "UILabel+DrawTextAlignment.h"

#define KTitleLabelCenter_Y  ((isIPhonePlat)?10:80)

#define fontSize    (isIPhonePlat?8:13)


@implementation SCDrawView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithStyle:(enum drawStyle)style frame:(CGRect)frame{

    self =[super initWithFrame:frame];
    if (self) {
        _style =style;
        _titleLabel =[[UILabel alloc] initWithFrame:CGRectZero];
        
        _titleLabel.textAlignment =TextAliCenter;
       
        
        _titleLabel.textColor =[UIColor blackColor];
        _titleLabel.backgroundColor =[UIColor clearColor];
        
        [self addSubview:_titleLabel];
        
    }
    return self;
};

-(void)setStyle:(enum drawStyle)style{
    _style =style;
   
}

-(void)setTitle:(NSString *)title{
    if (!_titleLabel.superview) {
        [self addSubview:_titleLabel];
    }
    _titleLabel.center =(CGPoint){self.bounds.size.width/2,KTitleLabelCenter_Y};
    _titleLabel.text =title;
  
    _titleLabel.font =[UIFont systemFontOfSize:fontSize];
    
    if (isIPhonePlat) {
         [_titleLabel sizeToFit];
    }else{
        _titleLabel.center =(CGPoint){self.bounds.size.width/2,KTitleLabelCenter_Y/2};
        CGRect frame =_titleLabel.frame;
        frame.size.height  =KTitleLabelCenter_Y;
        frame.size.width   =500;
        _titleLabel.frame =frame;
        _titleLabel.numberOfLines =0;
        _titleLabel.text =[NSString stringWithFormat:@"\n\n\n\n%@",title];
        [_titleLabel setVerticalAlignment:VerticalAlignmentBottom];
    }
    
   
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

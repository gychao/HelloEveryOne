//
//  SCNetManager.m
//  SleepCareII
//
//  Created by dilitech on 14-6-11.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCNetManager.h"
#import "AFNetworking.h"
#import "SVProgressHUD/SVProgressHUD.h"
#import "Vendor/DXAlertView.h"
#import "SCLoginVC.h"
#import "SCLoginLogicManager.h"
#import "SCRootViewController.h"


enum errorCode {
    SERVER_INTERNAL_ERROR = -999,  //服务器内部错误
    SERVER_SESSION_TIMEOUT= -998,  //服务器session 超时，需要重新登录
    ACCESS_VIOLATION      = -997,  //非法访问，key 错误
    OBJECT_CORRECT        = -996,  //返回的对象，非空且有效
    LOGIN_SUCCESS         = -1000, //登录成功
    WRONG_PASSWORD        = -1001, //密码错误
    USER_NOT_EXISTS       = -1002, //用户名不存在
    DELETE_REMIND_SUCCESS = -1100, //删除提醒成功
    DELETE_REMIND_FAILED  = -1101, // 删除提醒失败
    REMIND_NOT_EXISS      = -1102, //提醒不存在
    FIND_PASSWORD_SUCCESS = -1200, //成功找到密码
    FIND_PASSWORD_FAILED  = -1201  // 没有找到密码
};





@interface SCNetManager(){
    AFHTTPClient *_defaultClient;
}
@property(nonatomic,retain)AFHTTPClient *defaultClient;

@end
@implementation SCNetManager
@synthesize defaultClient =_defaultClient;
+(SCNetManager *)shareSCNetManager{
    static dispatch_once_t once;
    static SCNetManager *shareManager;
    dispatch_once(&once, ^ { shareManager = [[self alloc] init]; });
    return shareManager;
}

-(AFHTTPClient *)defaultClient{
    _defaultClient =nil;
    if (_defaultClient==nil) {
        NSString *urlstring =[SCShareFunc getServerUrlString];
        NSURL *serverUrl =[NSURL URLWithString:urlstring];
        _defaultClient =[AFHTTPClient clientWithBaseURL:serverUrl];
    }
    return _defaultClient;
}


+(void)loginWithName:(NSString *)name andPassword:(NSString *)password{
    
//    //  just login if no net work for dev
//    //  should delete when publish
//    [SCLoginLogicManager changeWindowRootVCToAfterLogin:YES orToLoginVC:NO];
//    return;
//    //

    if (![SCShareFunc isNotEmptyStringOfObj:name]) {
        [(DXAlertView *)[[DXAlertView  alloc] initWithTitle:nil contentText:@"用户名非空" leftButtonTitle:@"好" rightButtonTitle:nil] show];
        return;
    }
    if (![SCShareFunc isNotEmptyStringOfObj:password]) {
        [(DXAlertView *)[[DXAlertView  alloc] initWithTitle:nil contentText:@"密码非空" leftButtonTitle:@"好" rightButtonTitle:nil] show];
        return;
    }
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    // 获得此时 自动登录按钮的状态
    __weak  SCLoginVC * weekLogVC =(SCLoginVC *)[UIApplication sharedApplication].keyWindow.rootViewController;
    
    
//    NSString *urlstring =[SCShareFunc getServerUrlString];
//    NSURL *serverUrl =[NSURL URLWithString:urlstring];
//    NSLog(@"%@",urlstring);
    NSString *token =[[NSUserDefaults standardUserDefaults] objectForKey:@"Token"];
    NSDictionary *param;
    if ([SCShareFunc isNotEmptyStringOfObj:token]) {
        param =@{@"name":name,@"password":password,@"Token":token};
    }else{
        param =@{@"name":name,@"password":password};
    }
    
//    AFHTTPClient *client =[AFHTTPClient clientWithBaseURL:serverUrl];
//    [[SCNetManager shareSCNetManager] setDefaultClient:client];// =client;

    
    [[SCNetManager shareSCNetManager].defaultClient getPath:Klogin parameters:param success:^(AFHTTPRequestOperation *opration,id responseOBj){
       [SVProgressHUD dismiss];
        NSDictionary *responseDic =[NSJSONSerialization JSONObjectWithData:responseOBj options:NSJSONReadingMutableContainers error:nil];
        switch ([[responseDic objectForKey:KerrorCode] longValue]) {
            case LOGIN_SUCCESS:
            {
               NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:name forKey:KUserName];
                [defaults setObject:password forKey:KPassWord];
                [defaults setObject:[NSNumber numberWithBool:weekLogVC.autoLoginBtn.selected] forKey:KAutoLogin];
                [defaults synchronize];
                // logic 登录之后 需要获取 alluserinfo
                [self getAllUserInfo];
            }
                break;
            
            case WRONG_PASSWORD:{
                [[[DXAlertView alloc] initWithTitle:nil contentText:@"密码错误" leftButtonTitle:@"好" rightButtonTitle:nil] show];
            }break;
            case USER_NOT_EXISTS:{
                [[[DXAlertView alloc] initWithTitle:nil contentText:@"用户名不存在" leftButtonTitle:@"好" rightButtonTitle:nil] show];
            }break;
            default:{
                 [[[DXAlertView alloc] initWithTitle:nil contentText:@"数据返回异常" leftButtonTitle:@"好" rightButtonTitle:nil] show];
            }
                break;
        }
        
    } failure:^(AFHTTPRequestOperation * op, NSError *error){
        [SVProgressHUD dismiss];
        NSLog(@"error occ: %@, %d",error.userInfo,__LINE__);
        [(DXAlertView *)[[DXAlertView alloc] initWithTitle:nil contentText:@"网络连接失败" leftButtonTitle:@"好" rightButtonTitle:nil] show];
        
    }];

}

+(void)getPassWord:(void (^)(NSString *))succ loginName:(NSString *)loginName userName:(NSString *)userName{
  
    if (![SCShareFunc isNotEmptyStringOfObj:loginName]) {
        [[[DXAlertView alloc] initWithTitle:nil contentText:@"登录名非空" leftButtonTitle:@"好" rightButtonTitle:nil] show];
        return;
    }
    if (![SCShareFunc isNotEmptyStringOfObj:userName]) {
        [[[DXAlertView alloc] initWithTitle:nil contentText:@"使用名非空" leftButtonTitle:@"好" rightButtonTitle:nil] show];
        return;
    }
    
    if (![SVProgressHUD isVisible]) {
        [SVProgressHUD showWithStatus:@"loading" maskType:SVProgressHUDMaskTypeBlack];
    }
    NSDictionary *parma =@{@"loginName":loginName,@"userName":userName};
    
    [[SCNetManager shareSCNetManager].defaultClient getPath:KfindPassword parameters:parma success:^(AFHTTPRequestOperation *op,id response){
        [SVProgressHUD dismiss];
        NSDictionary *resPonseDic =[NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:nil];
        switch ([[resPonseDic objectForKey:KerrorCode] longValue]) {
            case FIND_PASSWORD_SUCCESS:
                succ([resPonseDic objectForKey:@"password"]);
                break;
            case FIND_PASSWORD_FAILED:{
                [[[DXAlertView alloc] initWithTitle:nil contentText:@"未找到密码" leftButtonTitle:@"好" rightButtonTitle:nil] show];
            }break;
            default:
                 [[[DXAlertView alloc] initWithTitle:nil contentText:@"数据返回异常" leftButtonTitle:@"好" rightButtonTitle:nil] show];
                break;
        }
        
    } failure:^(AFHTTPRequestOperation *op,NSError *err){
    
        [SVProgressHUD dismiss];
        [(DXAlertView *)[[DXAlertView alloc] initWithTitle:nil contentText:@"网络连接失败" leftButtonTitle:@"好" rightButtonTitle:nil] show];
    }];
}


+(void)getAllUserInfo{
    if (![SVProgressHUD isVisible]) {
        [SVProgressHUD showWithStatus:@"loading" maskType:SVProgressHUDMaskTypeBlack];
    }

    [[SCNetManager shareSCNetManager].defaultClient  getPath:KgetAllUserInfo parameters:nil success:^(AFHTTPRequestOperation *operation,id responseObj){
        [SVProgressHUD dismiss];
        NSDictionary *responseDic =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        
        switch ([[responseDic objectForKey:KerrorCode] longValue]) {
            case OBJECT_CORRECT:
            {
                //logic 得到的数据正常，需要切换window的 rootviewcontroller
                [SCLoginLogicManager  changeWindowRootVCToAfterLogin:YES orToLoginVC:NO];
                
                [(SCRootViewController *)[UIApplication sharedApplication].keyWindow.rootViewController displayAllUserInfoWithInfoDic:responseDic];
                
            }
                break;
            case SERVER_SESSION_TIMEOUT:{
                //logic 服务超时间，需要重新登录
                [SCLoginLogicManager  changeWindowRootVCToAfterLogin:NO  orToLoginVC:YES];
               [[[DXAlertView alloc] initWithTitle:nil contentText:@"服务超时,请重新登录" leftButtonTitle:@"好" rightButtonTitle:nil] show];
                
            }break;
                
            default: [[[DXAlertView alloc] initWithTitle:nil contentText:@"数据返回异常" leftButtonTitle:@"好" rightButtonTitle:nil] show];
                break;
        }

        
    } failure:^(AFHTTPRequestOperation * op, NSError *error){
        [SVProgressHUD dismiss];
        NSLog(@"error occ: %@, %d",error.userInfo,__LINE__);
        [(DXAlertView *)[[DXAlertView alloc] initWithTitle:nil contentText:@"网络连接失败" leftButtonTitle:@"好" rightButtonTitle:nil] show];
    }];
}


+(void)getSleepActivityInfoSuccess:(void (^)(BOOL, NSDictionary *))suceess faileture:(void (^)(BOOL))failture withType:(NSString *)type andWithPeroid:(int)period{
    [self shareSCNetManager].suceess =nil;
    [self shareSCNetManager].faileTure =nil;
    
    [self shareSCNetManager].suceess =suceess;
    [self shareSCNetManager].faileTure =failture;
    if (![SVProgressHUD isVisible]) {
        [SVProgressHUD showWithStatus:@"loading" maskType:SVProgressHUDMaskTypeBlack];
    }
    
    // NOTES: 次数userid 是用到的 ，如果没有用到，不是必须的
    NSString *userid =[[NSUserDefaults standardUserDefaults] objectForKey:KUserID];
    if (![SCShareFunc isNotEmptyStringOfObj:userid]) {
        return;
    }
    NSDictionary *parma;
    if ([type isEqualToString:KgetSleepActivity]) {
        parma =@{KUserID:userid,Kdate:globleDate};
    }else if ([type isEqualToString:KgetHeartbeatStatus]){
     parma =@{KUserID:userid,Kdate:globleDate};
    }else if ([type isEqualToString:KgetBreathStatus]){
         parma =@{KUserID:userid,Kdate:globleDate};
    }else if ([type isEqualToString:KgetSleepLevel]||[type isEqualToString:KgetSleepRange]){
         parma =@{KUserID:userid,Kdate:globleDate};
    }else if ([type isEqualToString:KgetCurrentSleepStatus]){
        parma =@{KUserID:userid};
    }else if ([type isEqualToString:KgetLast20MinSleepStatusDetail]){
          parma =@{KUserID:userid};
    }else if([type isEqualToString:kgetSleepRemind]){//  增加提醒 mqh
            parma =@{KUserID:userid};
    }else if([type isEqualToString:kgetPeriodBodyStatus]){// 历史数据 mqh  //logic 历史数据 始终传当前时间
        parma =@{KUserID:userid,Kdate:systemDate,Kperiod:[NSNumber numberWithInt:period]};
    }else if([type isEqualToString:kgetPeriodInBed]){// 历史数据 mqh
        parma =@{KUserID:userid,Kdate:systemDate,Kperiod:[NSNumber numberWithInt:period]};
    }else if([type isEqualToString:kgetPeriodSleepQuilty]){// 历史数据 mqh
        parma =@{KUserID:userid,Kdate:systemDate,Kperiod:[NSNumber numberWithInt:period]};
    }else if ([type isEqualToString:KgetMonthActiveDays]){
        parma =@{KUserID:userid,Kdate:riliMonthDay};
    }
    else{
        parma =nil;
    }
    
    [[SCNetManager shareSCNetManager].defaultClient getPath:type parameters:parma success:^(AFHTTPRequestOperation *op,id responseObj){
        [SVProgressHUD dismiss];
        NSDictionary *resPonseDic =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        switch ([[resPonseDic objectForKey:KerrorCode] longValue]) {
            case OBJECT_CORRECT:{
                NSLog(@"%@",[self shareSCNetManager].suceess);
               if ([self shareSCNetManager].suceess) {
                    [self shareSCNetManager].suceess(YES,resPonseDic);
                }
                [self shareSCNetManager].suceess =nil;
                [self shareSCNetManager].faileTure =nil;
            }
                break;
            case SERVER_SESSION_TIMEOUT:{
                //logic 服务超时间，需要重新登录
                [SCLoginLogicManager  changeWindowRootVCToAfterLogin:NO  orToLoginVC:YES];
                [[[DXAlertView alloc] initWithTitle:nil contentText:@"服务超时,请重新登录" leftButtonTitle:@"好" rightButtonTitle:nil] show];
            }
                break;
            default:{
                [[[DXAlertView alloc] initWithTitle:nil contentText:@"数据返回异常" leftButtonTitle:@"好" rightButtonTitle:nil] show];
                
                if ([self shareSCNetManager].faileTure) {
                    [self shareSCNetManager].faileTure(YES);
                }
                [self shareSCNetManager].suceess =nil;
                [self shareSCNetManager].faileTure =nil;
            }
                break;
        }
    } failure:^(AFHTTPRequestOperation * op,NSError * err){
        NSLog(@"%@",err.userInfo);
        [SVProgressHUD dismiss];
         [[[DXAlertView alloc] initWithTitle:nil contentText:@"网络链接失败" leftButtonTitle:@"好" rightButtonTitle:nil] show];
        if ([self shareSCNetManager].faileTure) {
            [self shareSCNetManager].faileTure(YES);
        }
        [self shareSCNetManager].suceess =nil;
        [self shareSCNetManager].faileTure =nil;
    }];
    
//    [SCNetManager shareSCNetManager].defaultClient =nil;
    
}
// 删除提醒。
+(void)deleteSleepRemindSuccess:(void (^)(BOOL success, NSDictionary * response))suceess
                      faileture:(void (^)(BOOL faileTure))failture withAry:(NSArray*)ary andCount:(int)count
{
    
    [self shareSCNetManager].suceess =nil;
    [self shareSCNetManager].faileTure =nil;
    
    [self shareSCNetManager].suceess =suceess;
    [self shareSCNetManager].faileTure =failture;
        if (![SVProgressHUD isVisible]) {
            [SVProgressHUD showWithStatus:@"loading" maskType:SVProgressHUDMaskTypeBlack];
        }
    
    // NOTES: 次数userid 是用到的 ，如果没有用到，不是必须的
    NSString *userid =[[NSUserDefaults standardUserDefaults] objectForKey:KUserID];
    if (![SCShareFunc isNotEmptyStringOfObj:userid]) {
        return;
    }
     NSMutableDictionary *parma=[NSMutableDictionary dictionary];
    [parma setObject:[NSNumber numberWithInt:ary.count] forKey:@"deleteCount"];
    //regId=1
    for(NSNumber * reqID in ary){
        [parma setObject:reqID forKey:@"regId"];
    }
    
    [[SCNetManager shareSCNetManager].defaultClient getPath:kdeleteSleepRemind parameters:parma success:^(AFHTTPRequestOperation *op,id responseObj){
          [SVProgressHUD dismiss];
        NSDictionary *resPonseDic =[NSJSONSerialization JSONObjectWithData:responseObj options:NSJSONReadingMutableContainers error:nil];
        switch ([[resPonseDic objectForKey:KerrorCode] longValue]) {
            case DELETE_REMIND_SUCCESS:{
                if ([self shareSCNetManager].suceess) {
                    [self shareSCNetManager].suceess(YES,resPonseDic);
                }
                if ([SCShareFunc systemVersonIsAfterNumber:7.0]) {
                    [self shareSCNetManager].suceess =nil;
                    [self shareSCNetManager].faileTure =nil;
                }
            }
                break;
            case DELETE_REMIND_FAILED:{
                //logic 服务超时间，需要重新登录
                [[[DXAlertView alloc] initWithTitle:nil contentText:@"删除失败,稍后尝试" leftButtonTitle:@"好" rightButtonTitle:nil] show];
                if ([SCShareFunc systemVersonIsAfterNumber:7.0]) {
                    [self shareSCNetManager].suceess =nil;
                    [self shareSCNetManager].faileTure =nil;
                }
            }
                break;
            default:{
//                [[[DXAlertView alloc] initWithTitle:nil contentText:@"数据返回异常,请重新登录" leftButtonTitle:@"好" rightButtonTitle:nil] show];
                
                if ([self shareSCNetManager].faileTure) {
                    [self shareSCNetManager].faileTure(YES);
                }
                if ([SCShareFunc systemVersonIsAfterNumber:7.0]) {
                    [self shareSCNetManager].suceess =nil;
                    [self shareSCNetManager].faileTure =nil;
                }

            }
                break;
        }
    } failure:^(AFHTTPRequestOperation * op,NSError * err){
        NSLog(@"%@",err.userInfo);
           [SVProgressHUD dismiss];
//        [[[DXAlertView alloc] initWithTitle:nil contentText:@"网络链接失败" leftButtonTitle:@"好" rightButtonTitle:nil] show];
        if ([self shareSCNetManager].faileTure) {
            [self shareSCNetManager].faileTure(YES);
        }
        if ([SCShareFunc systemVersonIsAfterNumber:7.0]) {
            [self shareSCNetManager].suceess =nil;
            [self shareSCNetManager].faileTure =nil;
        }
    }];


}

@end

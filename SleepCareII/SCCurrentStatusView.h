//
//  SCCurrentStatusView.h
//  SleepCareII
//
//  Created by dilitech on 14-6-16.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCCurrentStatusView : UIView



@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;



@property (weak, nonatomic) IBOutlet UIImageView *breathImageView;


@property (weak, nonatomic) IBOutlet UIImageView *inBedImageView;


@property (weak, nonatomic) IBOutlet UIImageView *heartbeatimageViewe;
@property (weak, nonatomic) IBOutlet UILabel *breath;
@property (weak, nonatomic) IBOutlet UILabel *heartbeat;



#pragma mark -当前
@property (weak, nonatomic) IBOutlet UIView *currentView;



#pragma mark - tableview top
@property (weak, nonatomic) IBOutlet UILabel *TopLabelTime;

@property (weak, nonatomic) IBOutlet UILabel *TopLabelHeartBeat;

@property (weak, nonatomic) IBOutlet UILabel *TopLabelBreath;

@property (weak, nonatomic) IBOutlet UILabel *TopLabelBreathStop;


#pragma mark - last20
@property(nonatomic,strong)UITableView *_20minTableView;
@property(nonatomic,strong)NSArray * dataArr;


// response  pass nil 
-(void)displayViewUse:(NSDictionary *)reponse withNumTag:(int)numIndex;

@end

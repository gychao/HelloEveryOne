//
//  CalendarViewController.h
//  AnQuanRiXun
//

#import <Foundation/Foundation.h>
#import "VRGCalendarView.h"

@interface CalendarViewController : NSObject

+(UIPopoverController*)shareCalendarViewPopoverController;


@end

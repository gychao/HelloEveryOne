//
//  SCSleepEvaluateVC.m
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCSleepEvaluateVC.h"
#import "SCRootViewController.h"
#import "SleepStatus.h"
#import "SCSleepEStatusView.h"



@interface SCSleepEvaluateVC ()<UIScrollViewDelegate,zuoyoufanObey>{
    // from 1 to n
    int _scrollViewIndex;
}
@end

enum btnTag{
    status =100,
    detail =101
};

#define  KScrollViewViewNumber 5  // zt,xt,hx,sm,smjd
@implementation SCSleepEvaluateVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
}
-(UIScrollView *)scroView{
    // 初始化 配置
    NSLog(@"%@,%s",NSStringFromCGRect(self.view.bounds),__func__);
    if (!_scroView) {
        
        UIScrollView * scrollView =[[UIScrollView alloc] initWithFrame:(CGRect){0,self.statusBtn.bounds.size.height,self.view.bounds.size.width,self.view.bounds.size.height -self.statusBtn.bounds.size.height}];
        scrollView.delegate =self;
        [self.view insertSubview:scrollView atIndex:0];
    
        NSString *viewName =nil;
        if ([SCShareFunc isIPhone]) {
            viewName =@"SleepStatus_iPhone";
        }else
            viewName =@"SleepStatus_iPad";
        
        SleepStatus * statusView;
        NSArray *viewArr =
        [[NSBundle mainBundle] loadNibNamed:viewName owner:nil options:nil];
        
        for (id view in viewArr) {
            if ([view isKindOfClass:[SleepStatus class]]) {
                statusView =view;
                break;
            }
        }
        
        //设置 蓝色外框
        CALayer *layer = [statusView  layer];
        layer.borderColor = [[UIColor colorWithRed:0.4824 green:0.6235 blue:0.7961 alpha:1] CGColor];
        layer.borderWidth = 0.5f;
        statusView.vcDelegate =self;
        
        // 设置statusview 背景色
        statusView.backgroundColor =[UIColor colorWithRed:0.9451 green:0.9451 blue:0.9451 alpha:1];
        
        statusView.frame =scrollView.bounds;
        [scrollView addSubview:statusView];
        statusView.tag =scrollView.contentOffset.y/scrollView.bounds.size.width +1;
        
        self.scroView =scrollView;
        [scrollView  setContentSize:(CGSize){KScrollViewViewNumber*scrollView.bounds.size.width,scrollView.bounds.size.height}];
        scrollView.pagingEnabled =YES;
        scrollView.showsVerticalScrollIndicator =NO;
        scrollView.backgroundColor=[UIColor clearColor];
        
    }
    return _scroView;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.visibleViews =[NSMutableArray array];
    
    // 默认配置
    self.statusBtn.selected =YES;
    self.detailsBtn.selected =NO;
    
    NSLog(@"%@,%s",NSStringFromCGRect(self.view.bounds),__func__);
 

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (_scrollViewIndex ==0) {
        _scrollViewIndex =1;
    }
    SleepStatus *sv =(SleepStatus *)[self.scroView viewWithTag:_scrollViewIndex];
    [sv displayViewUse:nil withNumTag:_scrollViewIndex];
}

-(void)dataToCalander{
    if (_scrollViewIndex ==0) {
        _scrollViewIndex =1;
    }
    SleepStatus *sv =(SleepStatus *)[self.scroView viewWithTag:_scrollViewIndex];
    [sv displayViewUse:nil withNumTag:_scrollViewIndex];

}

// 状态与详细的切换
- (IBAction)btnClickToChange:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    self.statusBtn.selected =self.detailsBtn.selected =NO;
    sender.selected =YES;
    switch (sender.tag) {
        case status:
        {
            [self.scroView setContentOffset:(CGPoint){0,0}];
        }
            break;
        case detail:{
            if (_scrollViewIndex==1) {
                _scrollViewIndex =2;
            }
            [self.scroView setContentOffset:(CGPoint){(_scrollViewIndex-1)*self.scroView.bounds.size.width,0}];
        }break;
            
        default:
            break;
    }
    
    [self LazyChangeScrollView:self.scroView OfItsView:nil];
    
}


/*!
 *  these func has deprecated
 */
//-(void)displayScrollViewWithDic:(NSDictionary *)dataDic{
//    // 根据scrollview 的偏移数量 来计算
//    int numTag =(self.scroView.contentOffset.y/self.scroView.bounds.size.width)+1;
//    SleepStatus *sleepView =(SleepStatus *)[self.scroView viewWithTag:numTag];
//    if (!sleepView) {
//        sleepView =self.visibleViews[0];
//    }
//    if (!sleepView) {
//        NSLog(@"not exists");
//        return;
//    }
//    NSString *identify =[self besureScrollViewIdentifybynumTag:numTag];
//    [sleepView displayItsViewUseData:dataDic andWithIdentify:identify andOtherParam:nil];
//    
//}
//
//-(NSString *)besureScrollViewIdentifybynumTag:(int)numTag{
//    NSString *identify =nil;
//    switch (numTag) {
//        case 1:
//            identify =statusIdentify;
//            break;
//        case 2:
//            identify =detail_xt;
//            break;
//        case 3:
//            identify =detail_hx;
//            break;
//        case 4:
//            identify =detail_sm;
//            break;
//        case 5:
//            identify =SleepLevel;
//            break;
//        default:
//            identify =statusIdentify;
//            break;
//    }
//    return identify;
//}


#pragma mark - scroll delegate
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        [self LazyChangeScrollView:scrollView OfItsView:nil];
    }
    NSInteger numIndex =
    (NSInteger) scrollView.contentOffset.x/scrollView.bounds.size.width+1;
     _scrollViewIndex =numIndex;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
     [self LazyChangeScrollView:scrollView OfItsView:nil];
    NSInteger numIndex =
    (NSInteger) scrollView.contentOffset.x/scrollView.bounds.size.width+1;
     _scrollViewIndex =numIndex;
}


-(void)LazyChangeScrollView:(UIScrollView *)scrollView OfItsView:(id)Para{
    
    //
 
    NSInteger numIndex =
    (NSInteger) scrollView.contentOffset.x/scrollView.bounds.size.width+1;
    
    
    
    self.statusBtn.selected =self.detailsBtn.selected =NO;
    if (numIndex==1) {
        self.statusBtn.selected =YES;
    }else if (numIndex >=2){
        self.detailsBtn.selected =YES;
    }else{
        self.statusBtn.selected =YES;
    }

    SleepStatus *csv =(SleepStatus *)[scrollView viewWithTag:numIndex];
    if (!csv) {
        
        [self.visibleViews removeAllObjects];
        for (id temView in self.scroView.subviews) {
            if ([temView isKindOfClass:[SleepStatus class]]) {
                [self.visibleViews insertObject:temView atIndex:0];
                [temView removeFromSuperview];
            }
            
        }
        
        if (self.visibleViews.count) {
            csv =self.visibleViews[0];
        }else{
            NSString *viewName =nil;
            if ([SCShareFunc isIPhone]) {
                viewName =@"SleepStatus_iPhone";
            }else{
                viewName =@"SleepStatus_iPad";
            }
          
            NSArray *viewArr =
            [[NSBundle mainBundle] loadNibNamed:viewName owner:nil options:nil];
            
            for (id view in viewArr) {
                if ([view isKindOfClass:[SleepStatus class]]) {
                    csv =view;
                    break;
                }
            }
            
        }
    }else{
        
        // 存在
        return;
    }
   
    csv.tag =numIndex;
    CGRect frame;
    frame.origin =(CGPoint){scrollView.contentOffset.x,0};
    frame.size =scrollView.bounds.size;
    csv.frame =frame;
    [scrollView addSubview:csv];
    
    [csv displayViewUse:nil withNumTag:numIndex];
    
    
}


- (IBAction)zuoyoufan:(UIButton *)sender {
    if (self.scroView.dragging||(!self.scroView.dragging&&self.scroView.decelerating)) {
        return;
    }
     int currenPage =_scrollViewIndex;
    int page =currenPage;
    if (sender.tag==100) {
        // 左翻
        page -=1;
    }else if(sender.tag ==101){
        page +=1;
    }
    
    if (page<1||page>KScrollViewViewNumber) {
            sender.hidden =YES;
            return;
            
    }
    [self.scroView setContentOffset:(CGPoint){self.scroView.bounds.size.width*(page-1),0}];
      
    [self LazyChangeScrollView:self.scroView OfItsView:nil];
    _scrollViewIndex =page;

}

#pragma mark -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

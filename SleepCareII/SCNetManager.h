//
//  SCNetManager.h
//  SleepCareII
//
//  Created by dilitech on 14-6-11.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - 接口名称
#define Klogin              @"login.action"                // 1#
#define KfindPassword       @"findPassword.action"         // 4#
#define KgetAllUserInfo     @"getAllUserInfo.action"

#define KgetSleepActivity   @"getSleepActivityInfo.action"  //3#
#define KgetHeartbeatStatus @"getHeartbeatStatus.action"    //8#
#define KgetBreathStatus    @"getBreathStatus.action"       //6#
#define KgetSleepLevel      @"getSleepLevel.action"         //14#
#define KgetSleepRange      @"getSleepRange.action"         //15#

#define KgetMonthActiveDays @"getMonthActiveDays.action"           //5# 日历活跃天数


#define kgetSleepRemind     @"getSleepRemind.action"        // 13#  mqh
#define kgetPeriodBodyStatus @"getPeriodBodyStatus.action"   // 获取历史 历史生命体征数据
#define kgetPeriodInBed     @"getPeriodInBed.action"    // 历史睡眠状态 mqh
#define kgetPeriodSleepQuilty  @"getPeriodSleepQuilty.action"  // 历史睡眠质量 mqh


//deleteSleepRemind.action?
#define kdeleteSleepRemind  @"deleteSleepRemind.action"     // 删除提醒 mqh

#define KgetCurrentSleepStatus @"getCurrentSleepStatus.action" //7#
#define KgetLast20MinSleepStatusDetail  @"getLast20MinSleepStatusDetail.action" //9#


#pragma mark - 接口返回关键字段
#define KerrorCode           @"errorCode"            //

@interface SCNetManager : NSObject

@property(nonatomic,strong)void (^ suceess)(BOOL success,NSDictionary * response);
@property(nonatomic,strong)void (^ faileTure)(BOOL faileTure);


+(SCNetManager *)shareSCNetManager;

#pragma mark - login refrence
+(void)loginWithName:(NSString *)name andPassword:(NSString *)password;
+(void)getPassWord:(void(^)(NSString * password))succ loginName:(NSString  *)loginName userName:(NSString *)userName;


#pragma mark - afterLogin
+(void)getAllUserInfo;


//  it has change userid can nil because userid have store in userdefault;
//  current status item can use this func
// period just fit some  interface not any ,you can past (0,7,14,28)
+(void)getSleepActivityInfoSuccess:(void (^)(BOOL success, NSDictionary * response))suceess faileture:(void (^)(BOOL faileTure))failture withType:(NSString *)type andWithPeroid:(int)period;

#pragma mark - currentStatus
//+(void)getCurrentSleepStatusSuccess:(void(^)(BOOL success,NSDictionary *response))suceess f

// 获取提醒列表


+(void)deleteSleepRemindSuccess:(void (^)(BOOL success, NSDictionary * response))suceess
                      faileture:(void (^)(BOOL faileTure))failture withAry:(NSArray*)ary andCount:(int)count;





@end

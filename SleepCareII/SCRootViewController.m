//
//  SCRootViewController.m
//  SleepCareII
//
//  Created by dilitech on 14-6-11.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCRootViewController.h"
#import "SCNetManager.h"
#import "SCAllUserTableViewCell.h"

#import "SCSleepEvaluateVC.h"
#import "SCCurrentStatusVC.h"
#import "SCHistoryVC.h"
#import "SCTiXingVC.h"
#import "SCLoginLogicManager.h"

#import "CalendarViewController.h"

@interface SCRootViewController ()<UITabBarControllerDelegate,VRGCalendarViewDelegate>{
    UIPopoverController *_calendarViewController;
    UIControl *_iPCalendarControl;
}

@end

enum selfDisPlayType{
    getAllUserInfo =1,
    getOneUserbyClick =1<<1
};

enum buttonTag{
    smpj =100, //睡眠评价
    dqzt =101, // 当前状态
    ls   =102,
    tx   =103,
    sy   =104,
    rl   =105,   // 日历
    zx   =106   // 注销
};

#define KCoreTableWidth     ((isIPhone5)?(956/2):(780/2))
#define KcoreTableHight     ((isIPhone5)?(512/2):(512/2))

@implementation SCRootViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGRect bounds =[[UIScreen mainScreen] bounds];
    NSLog(@"%@,%@",NSStringFromCGRect(bounds),NSStringFromCGRect(self.view.frame));
    
    //adjust to iPhone，temp
    if ([SCShareFunc isIPhone]) {
        if ([SCShareFunc systemVersonIsAfterNumber:7.0]) {
            
        }else{
            CGRect frame =  self.leftControlView.frame;
            frame.origin.y  =-20;
            self.leftControlView.frame =frame;
        }
        
       
//        CGRect frame =self.coreTableBgView.frame;
//        frame.size.width = KCoreTableWidth;
//        frame.size.height   =KcoreTableHight;
//        self.coreTableBgView.frame =frame;
        
   
    }

}
-(SCAllUserTableView *)userInfoTableView{
    if (_userInfoTableView==nil) {
        NSString *nibName =nil;
        if ([SCShareFunc isIPhone]) {
            nibName =@"userInfoTableView_iPhone";
        }else{
            nibName =@"userInfoTableView_iPad";
        }
        NSArray *temarr =[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        for (id obj in temarr) {
            if ([obj isKindOfClass:[UITableView class]]) {
                _userInfoTableView =obj;
                break;
            }
        }
        // 赋予header
        if ([SCShareFunc isIPhone]) {
            nibName =@"userInfoCell_iPhone";
        }else{
            nibName =@"userInfoCell_iPad";
        }
        NSArray *tem =[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        for (id dx in tem) {
            if ([dx isMemberOfClass:[UIView class]]) {
                _userInfoTableView.tableHeaderView =dx;
                break;
            }
        }
        
        // 赋予背景图片 // 需要根据平台不同 来设置
        _userInfoTableView.backgroundColor =[UIColor clearColor];
        
        _userInfoTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
        
        if ([SCShareFunc isIPhone]) {
            _userInfoTableView.backgroundView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zhuhuamian-kuang-960"]]; // it can resize  to _.backgroundView
            
        }else{
            _userInfoTableView.backgroundView  =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"beijingLogin"]];
        }
        
    }
    return _userInfoTableView;
}


#pragma mark - 开始登录进来
-(void)displayAllUserInfoWithInfoDic:(NSDictionary *)infoDic{
    self.tabViewDataDic =infoDic;
    
    [self reDisPlaySelfWithType:getAllUserInfo];
    
   
}

-(void)reDisPlaySelfWithType:(enum selfDisPlayType)type{
    switch (type) {
        case getAllUserInfo:
        {
            //[self.view.subviews makeObjectsPerformSelector:@selector(setHidden:) withObject:[NSNumber numberWithBool:YES]];
            for (UIView *tempVew in self.view.subviews) {
                tempVew.hidden =YES;
            }
            
            self.bgImageView.hidden =NO;
            self.userInfoTableView.hidden =NO;
            if (!self.userInfoTableView.superview) {
                
                
                CGRect bounds =[[UIScreen mainScreen] bounds];
                NSLog(@"%@,%@",NSStringFromCGRect(bounds),NSStringFromCGRect(self.view.frame));
                float x =bounds.size.height/2;
                float y =(bounds.size.width -[[UIApplication sharedApplication] statusBarFrame].size.width)/2;
                // adjust to iphone
                if ([SCShareFunc isIPhone]) {
                    if ([SCShareFunc systemVersonIsAfterNumber:7.0]) {
                        y+=[[UIApplication sharedApplication] statusBarFrame].size.width;
                    }
                }
                self.userInfoTableView.center =(CGPoint){x,y};
                if (isIPhone5) {
                    CGRect bounds =   self.userInfoTableView.bounds;
                    bounds.size.width = 568-20*2;
                    self.userInfoTableView.bounds =bounds;
                
                }
                
                
                [self.view addSubview:self.userInfoTableView];
            }
            
            [self.userInfoTableView setDataSource:self];
            [self.userInfoTableView setDelegate:self];
            [self.userInfoTableView reloadData];
            
            // gyc notes 点击返回首页 
            [self.tabBarVC.view removeFromSuperview];
            self.tabBarVC =nil;
        }
            break;
        case getOneUserbyClick:{
            [self.userInfoTableView setDelegate:nil];
            [self.userInfoTableView setDataSource:nil];
            [self.userInfoTableView  removeFromSuperview];
        
            self.tabViewDataDic =nil;
            for (UIView *temView in self.view.subviews) {
                temView.hidden =NO;
            }
            [self changeTopLabelOrBtnFrameWithType:smpj];
        }break;
            
        default:
            break;
    }
}

#define KallUserInfoList  @"allUserInfoList"  // 接口返回 关键字段
#pragma mark - tableview datasource and delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (!self.tabViewDataDic) {
        return 0;
    }
    return  [[self.tabViewDataDic objectForKey:KallUserInfoList] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SCAllUserTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"userInfo"];
    if (!cell) {
        cell = [[SCAllUserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"userInfo"];
    }
    [cell displayUseDic:[[self.tabViewDataDic objectForKey:KallUserInfoList] objectAtIndex:indexPath.row]];
    cell.backgroundColor =[UIColor clearColor];
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"sssss");
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int row =  indexPath.row;
    NSDictionary *infoDic =  [[self.tabViewDataDic objectForKey:KallUserInfoList] objectAtIndex:row];
    NSString *userId =[infoDic objectForKey:KUserID];
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:KUserID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // 获取姓名
    NSString *userName =[infoDic objectForKey:@"userName"];
    if ([SCShareFunc isNotEmptyStringOfObj:userName]) {
        self.topUserNameLabel.text =userName;
        if (isIPhonePlat) {
            CGRect oriframe =self.topUserNameLabel.frame; // cishi gao
            [self.topUserNameLabel sizeToFit];
            CGRect toFrame =self.topUserNameLabel.frame;//ci shi chang
            toFrame.size.width +=2;
            toFrame.size.height =oriframe.size.height;
            self.topUserNameLabel.frame =toFrame;

        }
        
    }
    // 获取时间
    self.topDateLabel.text =[SCShareFunc dateStringToYearMonthDayFromDate:[NSDate date]];
    
    
    if (self.coreTableBgView.subviews.count>=1) {
        
        
    }else{
        // 此处需要 完善 填写
        UITabBarController *tabbar =[[UITabBarController alloc] init];
        tabbar.delegate =self;
        self.tabBarVC =tabbar;
        
        NSString *vcName_SE =nil;
        NSString *vcName_CS =nil;
        NSString *vcName_Hy =nil;
        NSString *vcName_TX =nil;
        if ([SCShareFunc isIPhone]) {
            vcName_SE =@"SCSleepEvaluate_iPhone";
            vcName_CS =@"SCCurrentStatusVC_iPhone";
            vcName_Hy =@"SCHistoryVC_iPhone";
            vcName_TX =@"SCTiXingVC_iPhone";
        }else{
            vcName_SE =@"SCSleepEvaluate_iPad";
            vcName_CS =@"SCCurrentStatusVC_iPad";
            vcName_Hy =@"SCHistoryVC_iPad";
            vcName_TX =@"SCTiXingVC_iPad";
        }
        
        SCSleepEvaluateVC *seVC =[[SCSleepEvaluateVC alloc] initWithNibName:vcName_SE bundle:nil];
        SCCurrentStatusVC *currVC =[[SCCurrentStatusVC alloc] initWithNibName:vcName_CS bundle:nil];
        SCHistoryVC *hisVC =[[SCHistoryVC alloc] initWithNibName:vcName_Hy bundle:nil];
        SCTiXingVC *txVC =[[SCTiXingVC alloc] initWithNibName:vcName_TX bundle:nil];
        
        UINavigationController * navtx =[[UINavigationController alloc]initWithRootViewController:txVC];
        NSArray *vcArr =@[seVC,currVC,hisVC,navtx];
        tabbar.viewControllers =vcArr;
        
        
        tabbar.view.frame =self.coreTableBgView.bounds;
        [self.coreTableBgView addSubview:tabbar.view];
        tabbar.delegate =self;
        
    }
    [self.tabBarVC setSelectedIndex:0];
    [self deSelectleftControlBtn];
    [(UIButton *)[self.leftControlView viewWithTag:smpj] setSelected:YES];
    
    [self reDisPlaySelfWithType:getOneUserbyClick];

}
#pragma mark - 双击cell
// 双击cell 此方法已经 被废弃，改用选择 cell 就可以进入
-(void)clickTwiceAtTheCell:(SCAllUserTableViewCell *)cell{
    int row =  [[self.userInfoTableView  indexPathForCell:cell] row];
    NSDictionary *infoDic =  [[self.tabViewDataDic objectForKey:KallUserInfoList] objectAtIndex:row];
    NSString *userId =[infoDic objectForKey:KUserID];
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:KUserID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // 获取姓名
    NSString *userName =[infoDic objectForKey:@"userName"];
    if ([SCShareFunc isNotEmptyStringOfObj:userName]) {
        self.topUserNameLabel.text =userName;
    }
    // 获取时间
    self.topDateLabel.text =[SCShareFunc dateStringToYearMonthDayFromDate:[NSDate date]];
    
    
    if (self.coreTableBgView.subviews.count>=1) {
        
        
    }else{
        // 此处需要 完善 填写
        UITabBarController *tabbar =[[UITabBarController alloc] init];
        tabbar.delegate =self;
        self.tabBarVC =tabbar;
        
        NSString *vcName_SE =nil;
        NSString *vcName_CS =nil;
        NSString *vcName_Hy =nil;
        NSString *vcName_TX =nil;
        if ([SCShareFunc isIPhone]) {
            vcName_SE =@"SCSleepEvaluate_iPhone";
            vcName_CS =@"SCCurrentStatusVC_iPhone";
            vcName_Hy =@"SCHistoryVC_iPhone";
            vcName_TX =@"SCTiXingVC_iPhone";
        }else{
            vcName_SE =@"SCSleepEvaluate_iPad";
            vcName_CS =@"SCCurrentStatusVC_iPad";
            vcName_Hy =@"SCHistoryVC_iPad";
            vcName_TX =@"SCTiXingVC_iPad";
        }
        
        SCSleepEvaluateVC *seVC =[[SCSleepEvaluateVC alloc] initWithNibName:vcName_SE bundle:nil];
        SCCurrentStatusVC *currVC =[[SCCurrentStatusVC alloc] initWithNibName:vcName_CS bundle:nil];
        SCHistoryVC *hisVC =[[SCHistoryVC alloc] initWithNibName:vcName_Hy bundle:nil];
        SCTiXingVC *txVC =[[SCTiXingVC alloc] initWithNibName:vcName_TX bundle:nil];
        
        UINavigationController * navtx =[[UINavigationController alloc]initWithRootViewController:txVC];
        NSArray *vcArr =@[seVC,currVC,hisVC,navtx];
        tabbar.viewControllers =vcArr;
        
        
        tabbar.view.frame =self.coreTableBgView.bounds;
        [self.coreTableBgView addSubview:tabbar.view];
        tabbar.delegate =self;
        
    }
    [self.tabBarVC setSelectedIndex:0];
    [self deSelectleftControlBtn];
    [(UIButton *)[self.leftControlView viewWithTag:smpj] setSelected:YES];
    
    [self reDisPlaySelfWithType:getOneUserbyClick];
    
    
}
#pragma mark - 点击左侧控制按钮 和 日历
- (IBAction)leftControlViewBtnClick:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    if (sender.tag ==rl) {
        // 日历
        if (![SCShareFunc isIPhone]) {
             _calendarViewController=[CalendarViewController shareCalendarViewPopoverController];
             [_calendarViewController presentPopoverFromRect:CGRectMake(40, 6, 10, 30) inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
            VRGCalendarView* view=(VRGCalendarView*)_calendarViewController.contentViewController.view;

            view.delegate=self;
            
            

        }else{
            // iPhone
            if (!_iPCalendarControl) {
                _iPCalendarControl =[[UIControl alloc] initWithFrame:(CGRect){0,0,KDeviceHight,KDeviceWidth}];
                NSLog(@"%@",NSStringFromCGRect(_iPCalendarControl.frame));
                _iPCalendarControl.backgroundColor =[UIColor clearColor];
                VRGCalendarView *calendar =  [[VRGCalendarView alloc] init];// initWithFrame:(CGRect){KDeviceHight-350,self.topCalanderBtn.frame.origin.y+self.topCalanderBtn.frame.size.height+10,320,500}]; //default 320,200
                calendar.tag =100;
                calendar.delegate =self;
                [_iPCalendarControl addSubview:calendar];
                [_iPCalendarControl addTarget:self action:@selector(dismissCalandar:) forControlEvents:UIControlEventTouchUpInside];
            }
            if (!_iPCalendarControl.superview) {
                [self.view addSubview:_iPCalendarControl];
                [[_iPCalendarControl viewWithTag:100] setNeedsDisplay];
            }
       }
        return;
    }else if(sender.tag ==sy){
        //首页
        [self coverToOrignDate];
        sender.selected =NO;
        [SCNetManager getAllUserInfo];
        return;
        
    }// shouye
    
    [self deSelectleftControlBtn];
    sender.selected =YES;
    
    switch (sender.tag) {

        case sy:{
           
        }break;
        case zx:{
            // 注销
            [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:KAutoLogin];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [SCLoginLogicManager changeWindowRootVCToAfterLogin:NO orToLoginVC:YES];
        
        }break;
        default:{
            [self changeTopLabelOrBtnFrameWithType:sender.tag];
            [self.tabBarVC setSelectedIndex:(sender.tag -smpj)];
            [self tabBarController:self.tabBarVC didSelectViewController:self.tabBarVC.selectedViewController];
        }
            break;
    }
    // 默认恢复到原始状态
    [self coverToOrignDate];

}

-(void)dismissCalandar:(UIControl *)control{
    if (control.superview) {
        [control removeFromSuperview];
    }
}

-(void)coverToOrignDate{
    
    double interVal =-24*60*60;
    globleDate = [SCShareFunc stringFromDate:[NSDate dateWithTimeInterval:interVal sinceDate:[NSDate date]]]; //默认比系统时间 早一天 因为logic 数据计算都是从前一天的12:00到今天的12:00
    systemDate =[SCShareFunc stringFromDate:[NSDate date]];
    
     self.topDateLabel.text =(NSString *)[SCShareFunc dateStringToYearMonthDayFromDate:[NSDate date]];
}

-(void)deSelectleftControlBtn{
    for (id btn in self.leftControlView.subviews) {
        if ([btn isKindOfClass:[UIButton class]]) {
            [(UIButton *)btn setSelected:NO];
        }
    }
}
#define KInterVal ((isIPhonePlat)?5:10)  //日历按钮 和datelabel 间隔
-(void)changeTopLabelOrBtnFrameWithType:(enum buttonTag)tag{
    switch (tag) {
            NSLog(@"%s,%@,%@",__func__,NSStringFromCGRect(self.view.bounds),NSStringFromCGRect([UIScreen mainScreen].bounds));
        case smpj:
        {
            self.topCalanderBtn.hidden =NO;
            self.topDateLabel.frame =(CGRect){self.topCalanderBtn.frame.origin.x-KInterVal-self.topDateLabel.bounds.size.width,self.topCalanderBtn.frame.origin.y,self.topDateLabel.bounds.size.width,self.topDateLabel.bounds.size.height};
            self.topUserNameLabel.frame =(CGRect){self.topDateLabel.frame.origin.x-1-self.topUserNameLabel.bounds.size.width,self.topCalanderBtn.frame.origin.y,self.topUserNameLabel.bounds.size.width,self.topUserNameLabel.bounds.size.height};
            self.quiteBtn.frame =(CGRect){self.topUserNameLabel.frame.origin.x-1-self.quiteBtn.bounds.size.width,self.topCalanderBtn.frame.origin.y,self.quiteBtn.bounds.size.width,self.quiteBtn.bounds.size.height};
        }
            break;
        default:{
            self.topCalanderBtn.hidden =YES;
            self.topDateLabel.frame =(CGRect){self.view.bounds.size.width-self.topDateLabel.bounds.size.width,self.topCalanderBtn.frame.origin.y,self.topDateLabel.bounds.size.width,self.topDateLabel.bounds.size.height};
            self.topUserNameLabel.frame =(CGRect){self.topDateLabel.frame.origin.x-1-self.topUserNameLabel.bounds.size.width,self.topCalanderBtn.frame.origin.y,self.topUserNameLabel.bounds.size.width,self.topUserNameLabel.bounds.size.height};
            self.quiteBtn.frame =(CGRect){self.topUserNameLabel.frame.origin.x-1-self.quiteBtn.bounds.size.width,self.topCalanderBtn.frame.origin.y,self.quiteBtn.bounds.size.width,self.quiteBtn.bounds.size.height};
        }
            break;
    }
}

#pragma mark - tabbar delegate
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
//    double interVal =-24*60*60;
//    globleDate = [SCShareFunc stringFromDate:[NSDate dateWithTimeInterval:interVal sinceDate:[NSDate date]]]; //默认比系统时间 早一天 因为logic 数据计算都是从前一天的12:00到今天的12:00
//    
//    systemDate =[SCShareFunc stringFromDate:[NSDate date]];
    
    
}


#pragma mark - calenderdelegate
-(void)calendarView:(VRGCalendarView *)calendarView dateSelected:(NSDate *)date{
    
    if ([date timeIntervalSinceNow]>0) { //如果选择的是未来的时间，过滤掉
        return;
    }
    globleDate =[SCShareFunc stringFromDate:date];
    self.topDateLabel.text =(NSString *)[SCShareFunc dateStringToYearMonthDayFromDate:date];
    if ([SCShareFunc isIPhone]) {
        
        [_iPCalendarControl removeFromSuperview];
        
    }else{
        [_calendarViewController dismissPopoverAnimated:YES];
    }
    
    [(SCSleepEvaluateVC *)self.tabBarVC.selectedViewController  dataToCalander];
    
    

}

-(void)calendarView:(VRGCalendarView *)calendarView switchedToMonth:(int)month targetHeight:(float)targetHeight animated:(BOOL)animated{
    
    NSLog(@"%s,%@,select date :%@,labeltitle:%@",__func__,[NSString stringWithFormat:@"%@",calendarView.currentMonth],[NSString stringWithFormat:@"%@",calendarView.selectedDate],calendarView.labelCurrentMonth.text);

    if ([SCShareFunc isIPhone]) {
        CGRect frame = [[_iPCalendarControl viewWithTag:100] frame];
        frame.size.height =targetHeight;
        [[_iPCalendarControl viewWithTag:100] setFrame:(CGRect){KDeviceHight-frame.size.width-10,self.topCalanderBtn.frame.origin.y+self.topCalanderBtn.frame.size.height,frame.size.width,frame.size.height}]; //;
    }else{
     _calendarViewController.popoverContentSize=CGSizeMake(321, targetHeight);
    }
    
    // then get the active day
    //  getMonthActiveDays //2014-01-01 eg
    riliMonthDay =[SCShareFunc firstDayStrFromMonth:calendarView.currentMonth];
    NSLog(@"rilimonth --%@",riliMonthDay);
    
    [SCNetManager getSleepActivityInfoSuccess:^(BOOL success,NSDictionary *response){
        if (success) {
            [calendarView reDisplayViewUse:response];
        }
        
    } faileture:^(BOOL faile){
        if (faile) {
            [calendarView reDisplayViewUse:nil];
        }
        
    } withType:KgetMonthActiveDays andWithPeroid:0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)dealloc
{
    NSLog(@"%s",__func__);
}

@end

//
//  SCBaseViewController.m
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCBaseViewController.h"
#import "SCRootViewController.h"
#import "SCLoginVC.h"

@interface SCBaseViewController ()

@end

@implementation SCBaseViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
     
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ( [SCShareFunc systemVersonIsAfterNumber:7.0]) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.automaticallyAdjustsScrollViewInsets = YES;
    }
    
    self.view.backgroundColor =[UIColor clearColor];
    //djsfldsfdsfldkfds
//    
    [self hideTabBar];
    
    if (![self isMemberOfClass:[SCRootViewController class]]&&![self isMemberOfClass:[SCLoginVC class]]) {
        CGRect bouds =
        [(SCRootViewController *)[UIApplication sharedApplication].keyWindow.rootViewController coreTableBgView].bounds;
        NSLog(@"core bounds %@",NSStringFromCGRect(bouds));
        self.view.frame =(CGRect){0,0,bouds.size.width,bouds.size.height};
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)hideTabBar {

    if (!self.tabBarController) {
        return;
    }
    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
    
    if ([SCShareFunc systemVersonIsAfterNumber:7.0]) {
        self.tabBarController.tabBar.hidden =YES;
        return;
    }
    
    UIView *contentView;
    NSLog(@"%@",self.tabBarController.view.subviews);
    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] )
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    else
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height + self.tabBarController.tabBar.frame.size.height);
    NSLog(@"content frame %@",NSStringFromCGRect(contentView.frame));
    self.tabBarController.tabBar.hidden = YES;
    
}

#ifdef __IPHONE_6_0
-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}
#endif
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    NSLog(@"%s,%s",__func__,__FILE__);
}



-(void)dealloc{
    NSLog(@"%s,%s",__func__,__FILE__);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

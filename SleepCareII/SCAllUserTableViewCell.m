//
//  SCAllUserTableViewCell.m
//  SleepCareII
//
//  Created by dilitech on 14-6-12.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SCAllUserTableViewCell.h"
#import "SCRootViewController.h"

@implementation SCAllUserTableViewCell

#define ChengSe [UIColor colorWithRed:0.9725 green:0.3843 blue:0.1333 alpha:1]
#define LanSe   [UIColor colorWithRed:0.1412 green:0.4667 blue:0.7098 alpha:1]


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
 
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
        // Initialization code
    NSLog(@"这个cell 没有服用");
        
        NSString *nibName =nil;
        if ([SCShareFunc isIPhone]) {
            nibName =@"userInfoCell_iPhone";
        }else{
            nibName =@"userInfoCell_iPad";
        }
    NSArray *tem =[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    
    for (id dx in tem) {
        if ([dx isKindOfClass:[UITableViewCell class]]) {
            return dx;
        }
    }
//    self =[[[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil] firstObject];
    
//    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}
-(void)displayUseDic:(NSDictionary *)dic{

    self.userNameLabel.text =[dic objectForKey:@"userName"];
  
    // 💗
    [self userInfoChangeLabe:self.heartBeatLabel toText:[dic objectForKey:@"heartbeat"]];
    // 呼吸
    [self userInfoChangeLabe:self.breathLabel toText:[dic objectForKey:@"breath"]];
    // 呼吸暂停🌲
    [self userInfoChangeLabe:self.breathStopLabel toText:[dic objectForKey:@"apneaTime"]];
    // 睡眠质量
    [self userInfoChangeLabe:self.sleepQuaLabel toText:[dic objectForKey:@"quilty"]];
    // 警告 暂时没有用到
    
    //gyc logic 在床蓝色 离床橙色
    if ([[dic objectForKey:@"inBed"] boolValue]) {
        self.inBedImgView.image =[UIImage imageNamed:@"zaichuang-lan.png"];
        self.bgImageView.image =[UIImage imageNamed:@"moren"];
        self.userNameLabel.textColor  =  self.heartBeatLabel.textColor =self.breathLabel.textColor =self.breathStopLabel.textColor =self.sleepQuaLabel.textColor  =LanSe;
        
    }else{
        self.inBedImgView.image =[UIImage imageNamed:@"lichuang-cheng.png"];
        self.bgImageView.image =[UIImage imageNamed:@"Loginxuanzhong"];
        self.userNameLabel.textColor   =   self.heartBeatLabel.textColor =self.breathLabel.textColor =self.breathStopLabel.textColor  =self.sleepQuaLabel.textColor  =ChengSe;
    }
   

}

-(void)userInfoChangeLabe:(UILabel *)label toText:(id)text{
    if ([SCShareFunc isNotEmptyStringOfObj:text]) {
        label.text =text;
    }else{
        label.text =@"--";
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)clickUserInfoCellTwice:(UITapGestureRecognizer *)sender {
    
    NSLog(@"click");
   __weak UIView *temCell =self.superview;
    while (![temCell isKindOfClass:[UITableView class]]) {
        temCell =temCell.superview;
    }
    [(SCRootViewController *)((UITableView *)temCell).delegate   clickTwiceAtTheCell:(SCAllUserTableViewCell *)temCell];
    
}

- (void)dealloc
{
    NSLog(@"%s",__func__);
    [self removeGestureRecognizer:self.tapGes];
}

@end

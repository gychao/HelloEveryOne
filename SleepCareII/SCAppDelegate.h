//
//  SCAppDelegate.h
//  SleepCareII
//
//  Created by dilitech on 14-6-10.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

NSString * globleDate;
const NSString * systemDate;
const NSString * riliMonthDay;

BOOL isIPhonePlat;

@interface SCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

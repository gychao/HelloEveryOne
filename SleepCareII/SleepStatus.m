//
//  SleepStatus.m
//  SleepCareII
//
//  Created by dilitech on 14-6-13.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import "SleepStatus.h"
#import "SCSleepEvaluateVC.h"
#import "SCLinePrintView.h"

#import "UIButton+openTapArea.h"

#define KHightHeartBeat 140
#define KBreathRota     36

#define KanchorPoint  (isIPhonePlat?((CGPoint){119.0f/146.0f,0.5}):((CGPoint){117.0f/142.0f,0.5}))


@implementation SleepStatus

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)layoutSubviews{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
  
    self.zhuangTaiView.frame =(CGRect){0,0,self.frame.size.width,  self.frame.size.height};// zhuangtai
    self.heartBeatView.frame =(CGRect){0,0,self.frame.size.width,self.frame.size.height}; //xintiao
    self.breathView.frame =(CGRect){0,0,self.frame.size.width,self.frame.size.height};//呼吸
    self.sleepInfoView.frame =(CGRect){0,0,self.frame.size.width,self.frame.size.height};// sleepInf
    
    
    self.sleepRangeView.frame =(CGRect){0,0,self.frame.size.width,self.frame.size.height};  //jieduan
    
    
    [self addSubview:self.zhuangTaiView];
    [self addSubview:self.heartBeatView];
    [self addSubview:self.breathView];
    [self addSubview:self.sleepInfoView];
    [self addSubview:self.sleepRangeView];
    
    [self addSubview:self.zuofan];
    [self addSubview:self.youfan];
    

}

-(void)displayItsViewUseData:(NSDictionary *)dataDic andWithIdentify:(NSString *)identify andOtherParam:(id)pa{
    self.identify =identify;
    [self.subviews makeObjectsPerformSelector:@selector(setHidden:) withObject:@YES];
    
    if ([self.identify isEqualToString:statusIdentify]) {
        self.zhuangTaiView.hidden =NO;
        // 状态
        if (isIPhonePlat) {
            if (isIPhone5) {
                self.statusImageView.image =[UIImage imageNamed:@"_5zhuangtaih"];
            }
         
        }else{
            self.statusImageView.image =[UIImage imageNamed:@"beijingkuang.png"];
        }
        self.userInfoView.hidden =NO;
        [self.userInfoView drawSelfUseDataDic:dataDic withAnotherUsePara:nil];
        
        self.startDateLabel.text =globleDate;
        NSDate *endDate =[NSDate dateWithTimeInterval:24*60*60 sinceDate:[SCShareFunc dateFromString:globleDate]];
        self.endDateLabel.text =[SCShareFunc stringFromDate:endDate];
        self.startDateLabel.hidden =self.endDateLabel.hidden =NO;
        
        NSArray *LabelArr =@[_goSleepTime,_wakeUpTime,_outBedNum,_apneaTime,_dreamTime,_lowSleepTime,_deepSleepTime,_sleepTime,_dreamPercent,_lowSleepPercent,_deepSleepPercent];
        for (UILabel * label in LabelArr) {
            [self getText:label fromDataDic:dataDic];
        }
        
    }else if([self.identify isEqualToString:SleepLevel]){
         self.sleepRangeView.hidden =NO;
        self.zuofan.hidden =NO;
        
       
        self.sleepRangeView.vDesc   =@[@"1",@"2",@"3",@"4",@"5",@"6",@"7"];
        self.sleepRangeView.hDesc   =@[@"12:00",@"16:00",@"20:00",@"00:00",@"04:00",@"08:00",@"12:00"];
        [self.sleepRangeView setTitle:@"[ 非睡眠(7),醒/做梦(6、5),浅睡眠(4、3),深睡眠(2、1) ]"];
        [self.sleepRangeView  drawSelfUseDataDic:dataDic withAnotherUsePara:nil];
    }else if ([self.identify isEqualToString:detail_xt]){
        // 💗
        self.heartBeatView.hidden =NO;
        self.youfan.hidden =NO;
        // 平均心跳
        NSNumber *avHeartBeat =[dataDic objectForKey:@"avgHeartbeat"];
        if (avHeartBeat&&[avHeartBeat isKindOfClass:[NSNumber class]]&&[avHeartBeat intValue]>0) {
            self.AvarHeartLabel.text =[avHeartBeat stringValue];
        }
        // zuidixintiao
        NSNumber *lowBeat =[dataDic objectForKey:@"minHeartbeat"];
        if (lowBeat&&[lowBeat isKindOfClass:[NSNumber class]]&&[lowBeat intValue]>0) {
            self.lowHeartLeabel.text =[lowBeat stringValue];
//            self.lowHeartZhenImageView.transform =CGAffineTransformIdentity;
            
            self.lowHeartZhenImageView.layer.anchorPoint =KanchorPoint;
            float angle =[self getRotateTo:[lowBeat floatValue] andMaxNum:KHightHeartBeat];
            self.lowHeartZhenImageView.transform =CGAffineTransformMakeRotation(angle);
            self.lowHeartZhenImageView.center =self.lowHeartLeabel.center;
        }
        NSNumber *hightBeat =[dataDic objectForKey:@"maxHeartbeat"];
        if (hightBeat&&[hightBeat isKindOfClass:[NSNumber class]]&&[hightBeat intValue]>0) {
            self.hightHeartLabel.text =[hightBeat stringValue];
            self.hightHeartImageView.layer.anchorPoint =KanchorPoint;
            self.hightHeartImageView.center=self.hightHeartLabel.center;
            float angle =[self getRotateTo:[hightBeat floatValue] andMaxNum:KHightHeartBeat];
            self.hightHeartImageView.transform =CGAffineTransformMakeRotation(angle);
        }
    }else if([self.identify isEqualToString:detail_hx]){
        self.breathView.hidden =NO;
        self.zuofan.hidden =self.youfan.hidden =NO;
        id num =[dataDic objectForKey:@"apneaNum"];
        if (num&&[num isKindOfClass:[NSNumber class]]) {
            self.breathStop.text =[num stringValue];
        }
        num =[dataDic objectForKey:@"noBreathNum"];
        if (num&&[num isKindOfClass:[NSNumber class]]) {
            self.breathNoBreath.text =[num stringValue];
        }
        num =[dataDic objectForKey:@"lowBreathNum"];
        if (num&&[num isKindOfClass:[NSNumber class]]) {
            self.breathLowAir.text =[num stringValue];
        }
        num =[dataDic objectForKey:@"snoringTime"];
        if (num&&[num isKindOfClass:[NSString class]]) {
            self.breathDaHan.text =num;
        }
        
        
        num =[dataDic objectForKey:@"avgBreath"];
        if (num&&[num isKindOfClass:[NSNumber class]]&&[num intValue]>0) {
            self.breathLabel.text =[num stringValue];
            float angele =[self getRotateTo:[num intValue] andMaxNum:KBreathRota];
            self.breathZhiZhenView.layer.anchorPoint =KanchorPoint;
            self.breathZhiZhenView.center =self.breathLabel.center;
            self.breathZhiZhenView.transform =CGAffineTransformMakeRotation(angele);
        }
    }else if ([self.identify isEqualToString:detail_sm]){
    
        self.sleepInfoView.hidden =NO;
        self.zuofan.hidden =self.youfan.hidden =NO;
        
        id percent =[dataDic objectForKey:@"lowSleepPercent"];
        if ([SCShareFunc isNotEmptyStringOfObj:percent]) {
            self.qianShuiLabel.text =percent;
            CGFloat angle =[self getRotateFromPercent:percent];
            self.qianShuiZhiZhenView.center =self.qianShuiLabel.center;
            self.qianShuiZhiZhenView.layer.anchorPoint =KanchorPoint;
            self.qianShuiZhiZhenView.transform =CGAffineTransformMakeRotation(angle);
            
        }
        percent =[dataDic objectForKey:@"deepSleepPercent"];
        if ([SCShareFunc isNotEmptyStringOfObj:percent]) {
            self.shuShuiLabel.text =percent;
            CGFloat angel =[self getRotateFromPercent:percent];
            self.shuShuiZhiZhenView.center =self.shuShuiLabel.center;
            self.shuShuiZhiZhenView.layer.anchorPoint =KanchorPoint;
            self.shuShuiZhiZhenView.transform =CGAffineTransformMakeRotation(angel);
        }
    }

}


// 得到偏移量
-(CGFloat)getRotateTo:(CGFloat)toNum andMaxNum:(CGFloat)max{
    CGFloat angle =0;
    if (toNum<=0) {
        angle =0;
    }else if (toNum>max) {
        angle =M_PI;
    }else{
    
        angle =(toNum/max)*M_PI;
    }
    return angle;
}

-(CGFloat)getRotateFromPercent:(NSString *)percent{
    CGFloat angle =0;
    angle =[percent floatValue]*M_PI/100;
    return angle;
}

-(void)getText:(UILabel *)label fromDataDic:(NSDictionary *)dic{
    if (!label) {
        return;
    }
    NSString *key =[SCShareFunc nameWithInstance:label andInstanceOwner:self];
    NSString *value =[dic objectForKey:key];
    if (![SCShareFunc isNotEmptyStringOfObj:value]) {
        value =@"--";
    }
    label.text =value;
    label.hidden =NO;
}


-(void)displayViewUse:(NSDictionary *)reponse withNumTag:(int)numIndex{
    NSLog(@"ye");
    // 让这个csv 去加载
    NSString *type =nil;
    if (1==numIndex) {
        self.identify =statusIdentify;
        type =KgetSleepActivity;
    }else if (2==numIndex){
        self.identify =detail_xt;
        type =KgetHeartbeatStatus;
    }else if (3==numIndex){
        self.identify =detail_hx;
        type =KgetBreathStatus;
    }else if (4==numIndex){
        self.identify =detail_sm;
        type =KgetSleepLevel;
    }else{
        self.identify =SleepLevel;
        type =KgetSleepRange;
    }
    //gyc  先刷新一边 没有数据
    [self displayItsViewUseData:nil andWithIdentify:self.identify andOtherParam:nil];
    //
    
    
    [SCNetManager getSleepActivityInfoSuccess:^(BOOL secusss,NSDictionary *reponse){
        
        [self displayItsViewUseData:reponse andWithIdentify:self.identify andOtherParam:nil ];
        
    } faileture:^(BOOL failture){
        [self displayItsViewUseData:nil andWithIdentify:self.identify andOtherParam:nil];
    } withType:type andWithPeroid:0];
    
}

- (IBAction)zuoyoufan:(UIButton *)sender {
    if ([self.vcDelegate respondsToSelector:@selector(zuoyoufan:)]) {
        [self.vcDelegate zuoyoufan:sender];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
